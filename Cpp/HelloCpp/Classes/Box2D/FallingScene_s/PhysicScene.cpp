
#include "PhysicScene.h"
#include "AppMacros.h"

USING_NS_CC;
#define PTM_RATIO 32.0f
#define BOX2D_DEBUG

CCScene* PhysicScene::scene()
{
	
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    
    // 'layer' is an autorelease object
    PhysicScene *layer = PhysicScene::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

bool PhysicScene::init()
{
    // 1. super init first
    if ( !CCLayer::init() )
    {
        return false;
    }
    
    CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();

	// Create a world
	b2Vec2 gravity = b2Vec2(0.0f, -10.0f);
	this->m_b2world = new b2World(gravity);

	this->m_b2world->SetAllowSleeping(true);
	this->m_b2world->SetContinuousPhysics(true);

	// 視窗邊界
	b2BodyDef groundBodyDef;
	groundBodyDef.position.Set(visibleSize.width / 2 / PTM_RATIO, visibleSize.height / 2 / PTM_RATIO);
	
	b2Body* groundBody = this->m_b2world->CreateBody(&groundBodyDef);
	
	// Define the ground box shape.
	b2PolygonShape groundBox;
	// bottom
	groundBox.SetAsBox((visibleSize.width+2) / 2 / PTM_RATIO, 0, b2Vec2(0, (-visibleSize.height+2) / 2 / PTM_RATIO), 0);
	groundBody->CreateFixture(&groundBox, 100);
	// top
	groundBox.SetAsBox((visibleSize.width+2) / 2 / PTM_RATIO, 0, b2Vec2(0, (visibleSize.height+2) / 2 / PTM_RATIO), 0);
	groundBody->CreateFixture(&groundBox, 100);
	// left
	groundBox.SetAsBox(0, (visibleSize.height+2) / 2 / PTM_RATIO, b2Vec2((-visibleSize.width+2) / 2 / PTM_RATIO, 0), 0);
	groundBody->CreateFixture(&groundBox, 100);
	// right
	groundBox.SetAsBox(0, (visibleSize.height+2) / 2 / PTM_RATIO, b2Vec2((visibleSize.width+2) / 2 / PTM_RATIO, 0), 0);
	groundBody->CreateFixture(&groundBox, 100);
	
	// 開啟 touch 功能
	this->setTouchEnabled(true);

	// Add 'OnFrameMove' func to schedule to run in per frames
	schedule(schedule_selector(PhysicScene::OnFrameMove));
    return true;
}
void PhysicScene::OnFrameMove(float dt)
{
	int velocityIterations = 8;
	int positionIterations = 1;

	// Instruct the world to perform a single step of simulation. It is
	// generally best to keep the time step and iterations fixed.
	this->m_b2world->Step(dt, velocityIterations, positionIterations);

	for (b2Body *b = this->m_b2world->GetBodyList(); b; b = b->GetNext())
	{
		if (b->GetUserData() != NULL)
		{
			CCSprite *ballData = (CCSprite*)b->GetUserData();
			ballData->setPosition(ccp(b->GetPosition().x*PTM_RATIO , b->GetPosition().y*PTM_RATIO));
			ballData->setRotation( -1 * CC_RADIANS_TO_DEGREES(b->GetAngle()));
		}
	}

}
bool PhysicScene::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent)
{
	CCPoint p = this->ConvertPointToGL(pTouch);

	return true;
}
void PhysicScene::ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent)
{
	CCPoint p = this->ConvertPointToGL(pTouch);

}
void PhysicScene::ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent)
{
	CCPoint p = this->ConvertPointToGL(pTouch);

}

void PhysicScene::draw()
{
	/*
	// X軸 紅
	ccDrawColor4F(255, 0, 0, 0);
	for (int ix=0; ix<=1024; ix+=PTM_RATIO)
		ccDrawLine(ccp(ix,0.0f), ccp(ix,768.0f));
	// Y軸 藍
	ccDrawColor4F(0, 0, 255, 0);
	for (int iy=0; iy<=768; iy+=PTM_RATIO)
		ccDrawLine(ccp(0.0f,iy), ccp(1024.0f,iy));
	*/
#ifdef BOX2D_DEBUG
	// box2d debug draw
	b2DebugDraw *debugDraw = new b2DebugDraw(PTM_RATIO);
	uint32 flags = 0;
	flags += b2Draw::e_shapeBit;
	flags += b2Draw::e_jointBit;
	//flags += b2Draw::e_centerOfMassBit;
	//flags += b2Draw::e_aabbBit;
	//flags += b2Draw::e_pairBit;
	debugDraw->SetFlags(flags);

	this->m_b2world->SetDebugDraw(debugDraw);
	this->m_b2world->DrawDebugData();
#endif
}
void PhysicScene::registerWithTouchDispatcher()
{
     CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this, this->getTouchPriority(), true);
}
CCPoint PhysicScene::ConvertPointToGL(CCTouch *touches)
{
	CCPoint touchLocation = touches->getLocationInView();
	return CCDirector::sharedDirector()->convertToGL(touchLocation);
}
