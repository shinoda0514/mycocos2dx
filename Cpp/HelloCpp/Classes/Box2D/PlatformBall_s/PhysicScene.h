#ifndef __PHYSICWORLD_H__
#define __PHYSICWORLD_H__

#include "cocos2d.h"
#include "Box2D\Box2D.h"
#include "b2DebugDraw.h"

USING_NS_CC;
using namespace std;


class PhysicScene : public cocos2d::CCLayer
{
private:

public:

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  

	// box2d physic world
	b2World *m_b2world;

    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::CCScene* scene();
    
	// Ĳ�� API
	virtual void registerWithTouchDispatcher();
    virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
    virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
    virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	void CreateObject(const CCPoint &);

	void OnFrameMove(float dt);
	void draw();

	// convert coordinate system from view to GL
	CCPoint ConvertPointToGL(CCTouch* touches);

	CREATE_FUNC(PhysicScene);
};

#endif // __PHYSICWORLD_SCENE_H__
