
#include "HelloWorldScene.h"
#include "AppMacros.h"

USING_NS_CC;
#define PTM_RATIO 32.0f


//#include "MyNode.h"
#define BOX2D_DEBUG
//#define FREEHAND_DRAW

//#define SOFTBALL_BOUNCE
//#define SOFTBALL_ADD
//#define CREATE_POLY

//#define PULLEY_JOINT // 滑輪
#define REVOLUTE_JOINT // 旋轉
//#define PRISMATIC_JOINT // 位移
//#define GEAR_JOINT // 齒輪
#define MOUSE_JOINT // mouse joint

class QueryCallback : public b2QueryCallback
{
public:
	QueryCallback(const b2Vec2& point)
	{
		m_point = point;
		m_fixture = NULL;
	}

	bool ReportFixture(b2Fixture* fixture)
	{
		if (fixture->IsSensor()) return true; //ignore sensors

		bool inside = fixture->TestPoint(m_point);
		if (inside)
		{
			// We are done, terminate the query.
			m_fixture = fixture;
			return false;
		}

		// Continue the query.
		return true;
	}

	b2Vec2  m_point;
	b2Fixture* m_fixture;
};


CCScene* HelloWorld::scene()
{
	
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    
    // 'layer' is an autorelease object
    HelloWorld *layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

bool HelloWorld::init()
{
    // 1. super init first
    if ( !CCLayer::init() )
    {
        return false;
    }
    
    CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();

	// Create a world
	b2Vec2 gravity = b2Vec2(0.0f, -10.0f);
	this->m_b2world = new b2World(gravity);

	// Do we want to let bodies sleep?
	this->m_b2world->SetAllowSleeping(true);
	this->m_b2world->SetContinuousPhysics(true);

	//this->m_pMyNode = new MyNode();
	//this->m_pMyNode->init(m_b2world,12);

	// Set the joint as NULL.
	this->m_joint = NULL;

	// Set the body as empty for joint.
	b2BodyDef def;
	this->m_emptyBody = this->m_b2world->CreateBody(&def);
	
#ifdef FREEHAND_DRAW
	this->m_pMyDraw = new FreeHandDraw();
	this->m_pMyDraw->init(*this ,*m_b2world);
#endif
#ifdef CREATE_POLY
	// 創造多邊形的流程
	// 定義body
	b2BodyDef asd;
	asd.type = b2_staticBody;
	asd.position.Set(visibleSize.width / 2 / PTM_RATIO, visibleSize.height / 2 / PTM_RATIO);
	// 描述物體的形狀
	b2Vec2 vertices[4]; // 逆時針方向定義凸多邊形
	vertices[0].Set(0.0f, 0.0f);
	vertices[1].Set(1.0f, 0.0f);
	vertices[2].Set(0.0f, 1.0f);
	vertices[3].Set(-1.0f, 0.5f);
	int32 count = 4;
	
	b2PolygonShape asdPolygon;
	asdPolygon.Set(vertices, count);
	// 在physics world裡面宣告物體
	b2Body* asdBody = this->m_b2world->CreateBody(&asd);
	// 描述物體特性, 摩擦係數、密度、彈性係數, 做為shape與body的溝通橋梁
	b2FixtureDef asdFixture;
	asdFixture.shape = &asdPolygon;
	asdFixture.density = 1.0f;
	asdFixture.friction = 1.0f;
	asdFixture.restitution = 1.0f;
	asdBody->CreateFixture(&asdFixture);
#endif 
#ifdef SOFTBALL_BOUNCE
	this->m_pSoftball = new SoftBall();
	this->m_pSoftball->init();
	this->m_pSoftball->createPhysicsObject(m_b2world);
	this->addChild(this->m_pSoftball);
#endif

	// 視窗邊界
	b2BodyDef groundBodyDef;
	groundBodyDef.position.Set(visibleSize.width / 2 / PTM_RATIO, visibleSize.height / 2 / PTM_RATIO);

	b2Body* groundBody = this->m_b2world->CreateBody(&groundBodyDef);

	// Define the ground box shape.
	b2PolygonShape groundBox;

	// bottom
	groundBox.SetAsBox((visibleSize.width+2) / 2 / PTM_RATIO, 0, b2Vec2(0, (-visibleSize.height+2) / 2 / PTM_RATIO), 0);
	groundBody->CreateFixture(&groundBox, 100);

	// top
	groundBox.SetAsBox((visibleSize.width+2) / 2 / PTM_RATIO, 0, b2Vec2(0, (visibleSize.height+2) / 2 / PTM_RATIO), 0);
	groundBody->CreateFixture(&groundBox, 100);

	// left
	groundBox.SetAsBox(0, (visibleSize.height+2) / 2 / PTM_RATIO, b2Vec2((-visibleSize.width+2) / 2 / PTM_RATIO, 0), 0);
	groundBody->CreateFixture(&groundBox, 100);

	// right
	groundBox.SetAsBox(0, (visibleSize.height+2) / 2 / PTM_RATIO, b2Vec2((visibleSize.width+2) / 2 / PTM_RATIO, 0), 0);
	groundBody->CreateFixture(&groundBox, 100);

	// 開啟 touch 功能
	this->setTouchEnabled(true);

#ifdef GEAR_JOINT
	// Gear joint 實作
	b2CircleShape circleShape;
	circleShape.m_radius = 2.0f;
	b2BodyDef circleBodyDef;
	circleBodyDef.type = b2_dynamicBody;
	circleBodyDef.position.Set(5.0f, 12.0f);
	b2Body* myBody1 = this->m_b2world->CreateBody(&circleBodyDef);
	myBody1->CreateFixture(&circleShape, 1.0f);

	b2PolygonShape box;
	box.SetAsBox(0.5f, 5.0f);
	b2BodyDef polygonBodyDef;
	polygonBodyDef.type = b2_dynamicBody;
	polygonBodyDef.position.Set(7.5f, 12.0f);
	b2Body* myBody2 = this->m_b2world->CreateBody(&polygonBodyDef);
	myBody2->CreateFixture(&box, 1.0f);

	b2RevoluteJointDef revoluteJointDef;
	revoluteJointDef.Initialize(groundBody, myBody1, circleBodyDef.position);
	b2RevoluteJoint* myRevoluteJoint = (b2RevoluteJoint*)m_b2world->CreateJoint(&revoluteJointDef);

	b2PrismaticJointDef prismaticJointDef;
	prismaticJointDef.Initialize(groundBody, myBody2, polygonBodyDef.position, b2Vec2(0.0f, 1.0f));
	prismaticJointDef.lowerTranslation = -5.0f;
	prismaticJointDef.upperTranslation = 5.0f;
	prismaticJointDef.enableLimit = true;
	b2PrismaticJoint* myPrismaticJoint = (b2PrismaticJoint*)m_b2world->CreateJoint(&prismaticJointDef);

	b2GearJointDef gearJointDef;
	gearJointDef.bodyA = myBody1;
	gearJointDef.bodyB = myBody2;
	gearJointDef.joint1 = myRevoluteJoint;
	gearJointDef.joint2 = myPrismaticJoint;
	//gearJointDef.ratio = 2.0f * b2_pi / 100.0f;
	gearJointDef.ratio = -1.0f / circleShape.m_radius;
	this->m_b2world->CreateJoint(&gearJointDef);
#endif
#ifdef REVOLUTE_JOINT
	// 三角
	b2BodyDef triangleBodyDef;
	triangleBodyDef.type = b2_dynamicBody;
	triangleBodyDef.position.Set(visibleSize.width*0.5f/PTM_RATIO, visibleSize.height*0.5f/PTM_RATIO);
	triangleBodyDef.fixedRotation = false;

	b2Vec2 vertices[3];
	vertices[0].Set(1.0f, 0.0f);
	vertices[1].Set(0.0f, sqrtf(3.0f));
	vertices[2].Set(-1.0f, 0.0f);
	int32 count = 3;
	b2PolygonShape triangleShap;
	triangleShap.Set(vertices, count);
	b2FixtureDef triangleFixtureDef;
	triangleFixtureDef.density = 1.0f;
	triangleFixtureDef.shape = &triangleShap;

	b2Body *trianglebody = this->m_b2world->CreateBody(&triangleBodyDef);
	trianglebody->CreateFixture(&triangleFixtureDef);

	// 圓
	b2BodyDef circleBodyDef;
	circleBodyDef.type = b2_dynamicBody;
	circleBodyDef.position.Set(visibleSize.width*0.5f/PTM_RATIO, visibleSize.height*0.5f/PTM_RATIO);
	circleBodyDef.fixedRotation = false;

	b2CircleShape circleShap;
	circleShap.m_radius = 0.5f;
	b2FixtureDef circleFixtureDef;
	circleFixtureDef.shape = &circleShap;

	b2Body *circleBody = this->m_b2world->CreateBody(&circleBodyDef);
	circleBody->CreateFixture(&circleFixtureDef);
	
	// define revolute joint
	b2RevoluteJointDef jointDef;
	jointDef.bodyA = trianglebody;
	jointDef.bodyB = circleBody;
	jointDef.localAnchorA.Set(0.0f, sqrtf(3.0f) + 0.7f);
	jointDef.localAnchorB.Set(0.0f, 0.0f);
	jointDef.lowerAngle = -0.25f * M_PI; // 往下翻轉 , -45 degrees
	jointDef.upperAngle = 0.25f * M_PI; // 往上翻轉 , 45 degrees
	jointDef.enableLimit = true;

	// create joint
	//this->m_b2world->CreateJoint(&jointDef);
	this->m_RevoluteJoint = (b2RevoluteJoint*)this->m_b2world->CreateJoint(&jointDef);
	
#endif
#ifdef PULLEY_JOINT
	// Pulley joint 實作
	b2BodyDef circleBodyDef;
	//circleBodyDef.fixedRotation = true;
	circleBodyDef.type = b2_dynamicBody;
	circleBodyDef.position.Set(5.0f, 10.5f);
	b2Body* myBody1 = this->m_b2world->CreateBody(&circleBodyDef);

	b2BodyDef polygonBodyDef;
	//polygonBodyDef.fixedRotation = true;
	polygonBodyDef.type = b2_dynamicBody;
	polygonBodyDef.position.Set(10.0f, 10.5f);
	b2Body* myBody2 = this->m_b2world->CreateBody(&polygonBodyDef);

	b2CircleShape circleShape;
	circleShape.m_radius = 1.0f;
	myBody1->CreateFixture(&circleShape, 1);
	b2PolygonShape BoxShape;
	BoxShape.SetAsBox(1.0f, 1.0f); // 長 寬
	myBody2->CreateFixture(&BoxShape, 1);

	b2BodyDef groundBodyDef1;
	groundBodyDef1.position.Set(5.0f, 14.5f);
	b2Body* groundBody1 = this->m_b2world->CreateBody(&groundBodyDef1);
	b2BodyDef groundBodyDef2;
	groundBodyDef2.position.Set(10.0f, 14.5f);
	b2Body* groundBody2 = this->m_b2world->CreateBody(&groundBodyDef2);
	
	b2Vec2 anchor1 = myBody1->GetWorldCenter();
	b2Vec2 anchor2 = myBody2->GetWorldCenter();

	float32 ratio = 1.0f; // length1 + ratio * length2 == constant
	b2PulleyJointDef jointDef;
	jointDef.Initialize(myBody1, myBody2, groundBody1->GetWorldCenter(), groundBody2->GetWorldCenter(), anchor1, anchor2, ratio);

	this->m_b2world->CreateJoint(&jointDef);
#endif
#ifdef PRISMATIC_JOINT
	// Prismatic joint 實作
	b2BodyDef PrismaticBodyDef1;
	PrismaticBodyDef1.type = b2_staticBody;
	PrismaticBodyDef1.position.Set(5.0f, 4.5f);
	b2Body* myBodyA = this->m_b2world->CreateBody(&PrismaticBodyDef1);
	
	b2BodyDef PrismaticBodyDef2;
	PrismaticBodyDef2.type = b2_dynamicBody;
	PrismaticBodyDef2.position.Set( 10.5f, 4.5f);
	b2Body* myBodyB = this->m_b2world->CreateBody(&PrismaticBodyDef2);
	
	b2PolygonShape BoxShape1;
	BoxShape1.SetAsBox(1.0f, 1.0f);
	myBodyA->CreateFixture(&BoxShape1, 1);
	b2PolygonShape BoxShape2;
	BoxShape2.SetAsBox(1.0f, 1.0f);
	myBodyB->CreateFixture(&BoxShape2, 1);
	myBodyB->SetGravityScale(-1.0f);

	b2PrismaticJointDef jointDef;
	b2Vec2 worldAxis(0.0f, 1.0f);
	worldAxis.Normalize();
	jointDef.Initialize(myBodyA, myBodyB, myBodyA->GetWorldCenter(), worldAxis);
	jointDef.lowerTranslation = -5.0f;
	jointDef.upperTranslation = 12.5f;
	jointDef.enableLimit = true;
	jointDef.maxMotorForce = 1.0f;
	jointDef.motorSpeed = 0.0f;
	jointDef.enableMotor = true;
	
	this->m_b2world->CreateJoint(&jointDef);
#endif
	// Add 'OnFrameMove' func to schedule to run in per frames
	schedule(schedule_selector(HelloWorld::OnFrameMove));
	
    return true;
}
void HelloWorld::OnFrameMove(float dt)
{
	int velocityIterations = 8;
	int positionIterations = 1;
	
	// Instruct the world to perform a single step of simulation. It is
	// generally best to keep the time step and iterations fixed.
	this->m_b2world->Step(dt, velocityIterations, positionIterations);
	
	if (m_RevoluteJoint != NULL)
	{
		CCLog("ReactionForce X = %f, y = %f", m_RevoluteJoint->GetReactionForce(1/dt).x, m_RevoluteJoint->GetReactionForce(1/dt).y);
		if (fabs(m_RevoluteJoint->GetReactionForce(1/dt).x) >= 330.0f)
		{
			this->m_b2world->DestroyJoint(this->m_RevoluteJoint);
			this->m_RevoluteJoint = NULL;
			
			//b2RevoluteJoint* joint = (b2RevoluteJoint*)myWorld->CreateJoint(&jointDef);
			//... do stuff ...
			//myWorld->DestroyJoint(joint);
			//joint = NULL;
			
		}
	}
	
	for (b2Body *b = this->m_b2world->GetBodyList(); b; b = b->GetNext())
	{
		if (b->GetUserData() != NULL)
		{
			CCSprite *ballData = (CCSprite*)b->GetUserData();
			ballData->setPosition(ccp(b->GetPosition().x*PTM_RATIO , b->GetPosition().y*PTM_RATIO));
			ballData->setRotation( -1 * CC_RADIANS_TO_DEGREES(b->GetAngle()));
		}
	}
	//this->m_pMyNode->OnFrameMove(dt);
}
bool HelloWorld::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent)
{
	CCPoint p = this->ConvertPointToGL(pTouch);

#ifdef MOUSE_JOINT
	this->touchDown(b2Vec2(p.x / PTM_RATIO, p.y / PTM_RATIO));
#endif
#ifdef SOFTBALL_BOUNCE
	this->m_pSoftball->bounce();
#endif
#ifdef SOFTBALL_ADD
	SoftBall *Softball = new SoftBall();
	Softball->init();
	Softball->createPhysicsObject(m_b2world, pTouch->getLocation().x, pTouch->getLocation().y);
	this->addChild(Softball);
#endif

#ifdef FREEHAND_DRAW
	this->m_pMyDraw->touchBegan(pTouch);
#endif
	return true;
}
void HelloWorld::ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent)
{
	CCPoint p = this->ConvertPointToGL(pTouch);
#ifdef MOUSE_JOINT
	this->touchMove(b2Vec2(p.x / PTM_RATIO, p.y / PTM_RATIO));
#endif
#ifdef FREEHAND_DRAW
	this->m_pMyDraw->touchMoved(pTouch);
#endif
}
void HelloWorld::ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent)
{
	CCPoint p = this->ConvertPointToGL(pTouch);
#ifdef MOUSE_JOINT
	this->touchUp(b2Vec2(p.x / PTM_RATIO, p.y / PTM_RATIO));
#endif
#ifdef FREEHAND_DRAW
	this->m_pMyDraw->touchEnd();
#endif
}
bool HelloWorld::touchDown(const b2Vec2 &p)
{
	if (this->m_joint != NULL)
	{
		return false;
	}

	b2AABB aabb;
	b2Vec2 d;
	d.Set(0.001f, 0.001f);
	aabb.lowerBound = p - d;
	aabb.upperBound = p + d;
	QueryCallback callback(p);
	this->m_b2world->QueryAABB(&callback, aabb);
	if (callback.m_fixture)
	{
		b2Body *body = callback.m_fixture->GetBody();
		b2MouseJointDef md;
		md.bodyA = m_emptyBody;
		md.bodyB = body;
		md.target = p;
		md.maxForce = 750.0f * body->GetMass() / 2;
		this->m_joint = (b2MouseJoint *)(this->m_b2world->CreateJoint(&md));
		body->SetAwake(true);
		return true;
	}
	return false;
}
void HelloWorld::touchMove(const b2Vec2 &p)
{
	if(this->m_joint)
	{
		this->m_joint->SetTarget(p);
	}
}
void HelloWorld::touchUp(const b2Vec2 &p)
{
	if (this->m_joint)
	{
		this->m_b2world->DestroyJoint(this->m_joint);
		this->m_joint = NULL;
	}
}
void HelloWorld::draw()
{
#ifdef BOX2D_DEBUG
	// box2d debug draw
	b2DebugDraw *debugDraw = new b2DebugDraw(PTM_RATIO);
	uint32 flags = 0;
	flags += b2Draw::e_shapeBit;
	flags += b2Draw::e_jointBit;
	//flags += b2Draw::e_centerOfMassBit;
	//flags += b2Draw::e_aabbBit;
	//flags += b2Draw::e_pairBit;
	debugDraw->SetFlags(flags);

	this->m_b2world->SetDebugDraw(debugDraw);
	this->m_b2world->DrawDebugData();
#endif
}
void HelloWorld::registerWithTouchDispatcher()
{
     CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this, this->getTouchPriority(), true);
}
CCPoint HelloWorld::ConvertPointToGL(CCTouch *touches)
{
	//CCTouch *touch = (CCTouch*)touches->anyObject();
	CCPoint touchLocation = touches->getLocationInView();
	return CCDirector::sharedDirector()->convertToGL(touchLocation);
}
