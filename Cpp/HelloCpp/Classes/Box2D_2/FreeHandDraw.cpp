#include "FreeHandDraw.h"

#define VISIBLE_WIDTH 1024.0f
#define VISIBLE_HEIGHT 768.0f
#define PTM_RATIO 32.0f

FreeHandDraw::FreeHandDraw()
{
}
FreeHandDraw::~FreeHandDraw()
{
}
bool FreeHandDraw::init(CCLayer &layer, b2World &world)
{
	
	// free hand draw
	this->m_pCanvas = CCRenderTexture::create(VISIBLE_WIDTH, VISIBLE_HEIGHT, kCCTexture2DPixelFormat_RGBA8888);
    this->m_pCanvas->retain();
    this->m_pCanvas->setPosition(ccp(VISIBLE_WIDTH / 2, VISIBLE_HEIGHT / 2));
    
    layer.addChild(this->m_pCanvas);
    
	this->m_pBrush = CCSprite::create("Box2D/largeBrush.png");
    this->m_pBrush->retain();
	
	this->m_pWorld = &world; //
	this->m_pLayer = &layer; //

	return true;
}
void FreeHandDraw::addRectangleBetweenPointsToBody(b2Body *body, CCPoint start, CCPoint end)
{   
	float distance = ccpDistance(start, end);
    
    float sx=start.x;
    float sy=start.y;
    float ex=end.x;
    float ey=end.y;
    float dist_x=sx-ex;
    float dist_y=sy-ey;
    float angle= atan2(dist_y,dist_x);
    
    float px= (sx+ex)/2/PTM_RATIO - body->GetPosition().x;
    float py = (sy+ey)/2/PTM_RATIO - body->GetPosition().y;
    
    float width = abs(distance)/PTM_RATIO;
	float height = this->m_pBrush->boundingBox().size.height /PTM_RATIO;

	b2PolygonShape boxShape;
    boxShape.SetAsBox(width / 2, height / 2, b2Vec2(px,py),angle);
    
    b2FixtureDef boxFixtureDef;
    boxFixtureDef.shape = &boxShape;
    boxFixtureDef.density = 5;
    body->CreateFixture(&boxFixtureDef);
}
CCRect FreeHandDraw::getBodyRectangle(b2Body* body)
{
    CCSize s = CCDirector::sharedDirector()->getWinSize();
    
    float minX2 = s.width;
    float maxX2 = 0;
    float minY2 = s.height;
    float maxY2 = 0;
    
    const b2Transform& xf = body->GetTransform();
    for (b2Fixture* f = body->GetFixtureList(); f; f = f->GetNext())
    {
        b2PolygonShape* poly = (b2PolygonShape*)f->GetShape();
        int32 vertexCount = poly->m_vertexCount;
        b2Assert(vertexCount <= b2_maxPolygonVertices);
        
        for (int32 i = 0; i < vertexCount; ++i)
        {
            b2Vec2 vertex = b2Mul(xf, poly->m_vertices[i]);
            
            
            if(vertex.x < minX2)
            {
                minX2 = vertex.x;
            }
            
            if(vertex.x > maxX2)
            {
                maxX2 = vertex.x;
            }
            
            if(vertex.y < minY2)
            {
                minY2 = vertex.y;
            }
            
            if(vertex.y > maxY2)
            {
                maxY2 = vertex.y;
            }
        }
    }
    
    maxX2 *= PTM_RATIO;
    minX2 *= PTM_RATIO;
    maxY2 *= PTM_RATIO;
    minY2 *= PTM_RATIO;
    
    float width2 = maxX2 - minX2;
    float height2 = maxY2 - minY2;
    
    float remY2 = s.height - maxY2;
    
    return CCRectMake(minX2, remY2, width2, height2);
}
void FreeHandDraw::touchBegan(CCTouch *pTouch)
{
	plataformPoints.clear();

    CCPoint location = pTouch->getLocationInView();
    location = CCDirector::sharedDirector()->convertToGL(location);
    
	plataformPoints.push_back(location);
    
    previousLocation = location;
    
    b2BodyDef myBodyDef;
    myBodyDef.type = b2_staticBody;
    myBodyDef.position.Set(location.x/PTM_RATIO,location.y/PTM_RATIO);
	currentPlatformBody = this->m_pWorld->CreateBody(&myBodyDef);
}
void FreeHandDraw::touchMoved(CCTouch *pTouch)
{
	CCPoint start = pTouch->getLocationInView();
    start = CCDirector::sharedDirector()->convertToGL(start);
	CCPoint end = pTouch->getPreviousLocationInView();
    end = CCDirector::sharedDirector()->convertToGL(end);

	// canvas
	this->m_pCanvas->begin();
	float distance = ccpDistance(start, end);

    for (int i = 0; i < distance; i++)
    {
        float difx = end.x - start.x;
        float dify = end.y - start.y;
        float delta = (float)i / distance;
		this->m_pBrush->setPosition(ccp(start.x + (difx * delta), start.y + (dify * delta)));
        this->m_pBrush->visit();
	}
	this->m_pCanvas->end();
	
	// body
	float distance_2 = ccpDistance(start, previousLocation);
    if(distance_2 > 15)
    {
        addRectangleBetweenPointsToBody(currentPlatformBody, previousLocation, start);
		plataformPoints.push_back(start);
        previousLocation = start;
    }
}
void FreeHandDraw::touchEnd()
{
	b2BodyDef myBodyDef;
    myBodyDef.type = b2_dynamicBody;
    myBodyDef.position.Set(currentPlatformBody->GetPosition().x, currentPlatformBody->GetPosition().y); //set the starting position
	b2Body* newBody = this->m_pWorld->CreateBody(&myBodyDef);
    
    for(int i=0; i < plataformPoints.size()-1; i++)
    {
        CCPoint start = plataformPoints[i];
        CCPoint end = plataformPoints[i+1];
        addRectangleBetweenPointsToBody(newBody,start,end);
    }
    
    this->m_pWorld->DestroyBody(currentPlatformBody);
    
    
    CCSize s = CCDirector::sharedDirector()->getWinSize();

    CCRect bodyRectangle = getBodyRectangle(newBody);
    
    CCImage *pImage = this->m_pCanvas->newCCImage();
    CCTexture2D *tex = CCTextureCache::sharedTextureCache()->addUIImage(pImage,NULL);
    CC_SAFE_DELETE(pImage);
    //
    CCSprite *sprite = new CCSprite();
    sprite->initWithTexture(tex, bodyRectangle);

	float anchorX = newBody->GetPosition().x * PTM_RATIO - bodyRectangle.getMinX();
	float anchorY = bodyRectangle.size.height - (s.height - bodyRectangle.getMinY() - newBody->GetPosition().y * PTM_RATIO); //
    
    sprite->setAnchorPoint(ccp(anchorX / bodyRectangle.size.width,  anchorY / bodyRectangle.size.height));
    newBody->SetUserData(sprite);
    m_pLayer->addChild(sprite);
    //
	m_pLayer->removeChild(this->m_pCanvas,true);
    this->m_pCanvas->release();
    
    this->m_pCanvas = CCRenderTexture::create(s.width, s.height, kCCTexture2DPixelFormat_RGBA8888);
    this->m_pCanvas->retain();
    this->m_pCanvas->setPosition(ccp(s.width / 2, s.height / 2));
    
	m_pLayer->addChild(this->m_pCanvas, 5);
	
}