
#include "HelloWorldScene.h"
#include "AppMacros.h"

USING_NS_CC;
#define PTM_RATIO 32.0f

#define BOX2D_DEBUG


#define MOUSE_JOINT // mouse joint

class QueryCallback : public b2QueryCallback
{
public:
	QueryCallback(const b2Vec2& point)
	{
		m_point = point;
		m_fixture = NULL;
	}

	bool ReportFixture(b2Fixture* fixture)
	{
		if (fixture->IsSensor()) return true; //ignore sensors

		bool inside = fixture->TestPoint(m_point);
		if (inside)
		{
			// We are done, terminate the query.
			m_fixture = fixture;
			return false;
		}

		// Continue the query.
		return true;
	}

	b2Vec2  m_point;
	b2Fixture* m_fixture;
};


CCScene* HelloWorld::scene()
{
	
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    
    // 'layer' is an autorelease object
    HelloWorld *layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

bool HelloWorld::init()
{
    // 1. super init first
    if ( !CCLayer::init() )
    {
        return false;
    }
    
    CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();

	// Create a world
	b2Vec2 gravity = b2Vec2(0.0f, -10.0f);
	this->m_b2world = new b2World(gravity);

	// Do we want to let bodies sleep?
	this->m_b2world->SetAllowSleeping(true);
	this->m_b2world->SetContinuousPhysics(true);

	// Set the joint as NULL.
	this->m_joint = NULL;

	// Set the body as empty for joint.
	b2BodyDef def;
	this->m_emptyBody = this->m_b2world->CreateBody(&def);
	
#
	// 視窗邊界
	b2BodyDef groundBodyDef;
	groundBodyDef.position.Set(visibleSize.width / 2 / PTM_RATIO, visibleSize.height / 2 / PTM_RATIO);

	b2Body* groundBody = this->m_b2world->CreateBody(&groundBodyDef);

	// Define the ground box shape.
	b2PolygonShape groundBox;

	// bottom
	groundBox.SetAsBox((visibleSize.width+2) / 2 / PTM_RATIO, 0, b2Vec2(0, (-visibleSize.height+2) / 2 / PTM_RATIO), 0);
	groundBody->CreateFixture(&groundBox, 100);

	// top
	groundBox.SetAsBox((visibleSize.width+2) / 2 / PTM_RATIO, 0, b2Vec2(0, (visibleSize.height+2) / 2 / PTM_RATIO), 0);
	groundBody->CreateFixture(&groundBox, 100);

	// left
	groundBox.SetAsBox(0, (visibleSize.height+2) / 2 / PTM_RATIO, b2Vec2((-visibleSize.width+2) / 2 / PTM_RATIO, 0), 0);
	groundBody->CreateFixture(&groundBox, 100);

	// right
	groundBox.SetAsBox(0, (visibleSize.height+2) / 2 / PTM_RATIO, b2Vec2((visibleSize.width+2) / 2 / PTM_RATIO, 0), 0);
	groundBody->CreateFixture(&groundBox, 100);

	// 開啟 touch 功能
	this->setTouchEnabled(true);


	// Add 'OnFrameMove' func to schedule to run in per frames
	schedule(schedule_selector(HelloWorld::OnFrameMove));
	
    return true;
}
void HelloWorld::OnFrameMove(float dt)
{
	int velocityIterations = 8;
	int positionIterations = 1;
	
	// Instruct the world to perform a single step of simulation. It is
	// generally best to keep the time step and iterations fixed.
	this->m_b2world->Step(dt, velocityIterations, positionIterations);
	
	
	for (b2Body *b = this->m_b2world->GetBodyList(); b; b = b->GetNext())
	{
		if (b->GetUserData() != NULL)
		{
			CCSprite *ballData = (CCSprite*)b->GetUserData();
			ballData->setPosition(ccp(b->GetPosition().x*PTM_RATIO , b->GetPosition().y*PTM_RATIO));
			ballData->setRotation( -1 * CC_RADIANS_TO_DEGREES(b->GetAngle()));
		}
	}
}
bool HelloWorld::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent)
{
	CCPoint p = this->ConvertPointToGL(pTouch);

#ifdef MOUSE_JOINT
	this->touchDown(b2Vec2(p.x / PTM_RATIO, p.y / PTM_RATIO));
#endif

	return true;
}
void HelloWorld::ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent)
{
	CCPoint p = this->ConvertPointToGL(pTouch);
#ifdef MOUSE_JOINT
	this->touchMove(b2Vec2(p.x / PTM_RATIO, p.y / PTM_RATIO));
#endif

}
void HelloWorld::ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent)
{
	CCPoint p = this->ConvertPointToGL(pTouch);
#ifdef MOUSE_JOINT
	this->touchUp(b2Vec2(p.x / PTM_RATIO, p.y / PTM_RATIO));
#endif

}
bool HelloWorld::touchDown(const b2Vec2 &p)
{
	if (this->m_joint != NULL)
	{
		return false;
	}

	b2AABB aabb;
	b2Vec2 d;
	d.Set(0.001f, 0.001f);
	aabb.lowerBound = p - d;
	aabb.upperBound = p + d;
	QueryCallback callback(p);
	this->m_b2world->QueryAABB(&callback, aabb);
	if (callback.m_fixture)
	{
		b2Body *body = callback.m_fixture->GetBody();
		b2MouseJointDef md;
		md.bodyA = m_emptyBody;
		md.bodyB = body;
		md.target = p;
		md.maxForce = 750.0f * body->GetMass() / 2;
		this->m_joint = (b2MouseJoint *)(this->m_b2world->CreateJoint(&md));
		body->SetAwake(true);
		return true;
	}
	return false;
}
void HelloWorld::touchMove(const b2Vec2 &p)
{
	if(this->m_joint)
	{
		this->m_joint->SetTarget(p);
	}
}
void HelloWorld::touchUp(const b2Vec2 &p)
{
	if (this->m_joint)
	{
		this->m_b2world->DestroyJoint(this->m_joint);
		this->m_joint = NULL;
	}
}
void HelloWorld::draw()
{
#ifdef BOX2D_DEBUG
	// box2d debug draw
	b2DebugDraw *debugDraw = new b2DebugDraw(PTM_RATIO);
	uint32 flags = 0;
	flags += b2Draw::e_shapeBit;
	flags += b2Draw::e_jointBit;
	//flags += b2Draw::e_centerOfMassBit;
	//flags += b2Draw::e_aabbBit;
	//flags += b2Draw::e_pairBit;
	debugDraw->SetFlags(flags);

	this->m_b2world->SetDebugDraw(debugDraw);
	this->m_b2world->DrawDebugData();
#endif
}
void HelloWorld::registerWithTouchDispatcher()
{
     CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this, this->getTouchPriority(), true);
}
CCPoint HelloWorld::ConvertPointToGL(CCTouch *touches)
{
	//CCTouch *touch = (CCTouch*)touches->anyObject();
	CCPoint touchLocation = touches->getLocationInView();
	return CCDirector::sharedDirector()->convertToGL(touchLocation);
}
