#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include "Box2D\Box2D.h"


USING_NS_CC;
using namespace std;


class HelloWorld : public cocos2d::CCLayer
{
private:

	b2RevoluteJoint *m_RevoluteJoint;

	// Save the touch joint , for mouse joint
	b2MouseJoint *m_joint;
	b2Body *m_emptyBody;


public:

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  

	// box2d physic world
	b2World *m_b2world;

    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::CCScene* scene();
    
	// Ĳ�� API
	virtual void registerWithTouchDispatcher();
    virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
    virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
    virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);

	// for mouse joint control
	bool touchDown(const b2Vec2 &p);
	void touchMove(const b2Vec2 &p);
	void touchUp(const b2Vec2 &p);

	void OnFrameMove(float dt);
	void draw();

	// convert coordinate system from view to GL
	CCPoint ConvertPointToGL(CCTouch* touches);

    CREATE_FUNC(HelloWorld);
};

#endif // __HELLOWORLD_SCENE_H__
