
#include "SoftBall.h"
#define PTM_RATIO 32.0f

//#define IOS_PLATFORM
#define WINDOWS_PLATFORM

bool SoftBall::init()
{
	this->texture = CCTextureCache::sharedTextureCache()->addImage("Box2D/ball.png");

	return true;
}

void SoftBall::createPhysicsObject(b2World* world)
{
	pWorld=world;
	// Center is the position of the circle that is in the center (inner circle)
    b2Vec2 center = b2Vec2(240/PTM_RATIO, 160/PTM_RATIO);

	b2CircleShape circleShape;
    circleShape.m_radius = 0.1f;

	b2FixtureDef fixtureDef;
    fixtureDef.shape = &circleShape;
    fixtureDef.density = 0.1;
    fixtureDef.restitution = 0.05;
    fixtureDef.friction = 1.0;

	// Delta angle to step by
    this->m_fdeltaAngle = (2.f * M_PI) / NUM_SEGMENTS;

	// Radius of the wheel
    m_fRadius = 75;

	// Need to store the bodies so that we can refer back
    // to it when we connect the joints
    //bodies = [[NSMutableArray alloc] init];

	for (int i = 0; i < NUM_SEGMENTS; i++) 
	{
        // Current angle
        float theta = m_fdeltaAngle*i;
 
        // Calculate x and y based on theta
        float x = m_fRadius*cosf(theta);
        float y = m_fRadius*sinf(theta);
        // Remember to divide by PTM_RATIO to convert to Box2d coordinate
        b2Vec2 circlePosition = b2Vec2(x/PTM_RATIO, y/PTM_RATIO);
 
        b2BodyDef bodyDef;
        bodyDef.type = b2_dynamicBody;
        // Position should be relative to the center
        bodyDef.position = (center + circlePosition);
 
        // Create the body and fixture
        b2Body *body;
        body = world->CreateBody(&bodyDef);
        body->CreateFixture(&fixtureDef);
		
 
        // 將完成初始化的body放到list
		this->bodyList.push_back(body);
    }

	// Circle at the center (inner circle)
    b2BodyDef innerCircleBodyDef;
    // Make the inner circle larger
    innerCircleShape.m_radius = 1.0f;
	fixtureDef.shape = &innerCircleShape;
    innerCircleBodyDef.type = b2_dynamicBody;

	// Position is at the center
    innerCircleBodyDef.position = center;
    innerCircleBody = world->CreateBody(&innerCircleBodyDef);
    innerCircleFix = innerCircleBody->CreateFixture(&fixtureDef);

	// Connect the joints
    b2DistanceJointDef jointDef;
	
	// 元素、跌代器 (類似pointer概念)
	b2Body *curBody = this->bodyList.front(); // 跌帶器(pointer)指向元素, front 是指bodylist的第一個元素, begin 是指第一個跌帶器
	//this->bodyList

	// it != this->bodyList.end() 指向的元素為NULL , 所以 *(this->bodyList.end()) 會導致中斷
	for (list<b2Body*>::iterator it = this->bodyList.begin(); it != this->bodyList.end(); it++)
	{
		b2Body *nextBody;
		if (*it == this->bodyList.back())
			nextBody = this->bodyList.front();
		else
		{
			nextBody = *(++it);
			it--;
		}

		// Connect the outer circles to each other
		jointDef.Initialize(curBody, nextBody,
							curBody->GetWorldCenter(), 
							nextBody->GetWorldCenter() );
		
		// Specifies whether the two connected bodies should collide with each other
		jointDef.collideConnected = true;
		jointDef.frequencyHz = 0.0f;
		jointDef.dampingRatio = 0.0f;
 
		jointList.push_back(world->CreateJoint(&jointDef));
 
		// Connect the center circle with other circles        
		jointDef.Initialize(curBody, innerCircleBody, curBody->GetWorldCenter(), center);
		jointDef.collideConnected = true;
		jointDef.frequencyHz = 8.0;
		jointDef.dampingRatio = 0.5;
 
		jointList.push_back(world->CreateJoint(&jointDef));		
		curBody = nextBody;
	}
}
void SoftBall::createPhysicsObject(b2World* world, float fx, float fy)
{
	pWorld=world;
	// Center is the position of the circle that is in the center (inner circle)
    b2Vec2 center = b2Vec2(fx/PTM_RATIO, fy/PTM_RATIO);

	b2CircleShape circleShape;
    circleShape.m_radius = 0.1f;

	b2FixtureDef fixtureDef;
    fixtureDef.shape = &circleShape;
    fixtureDef.density = 0.1;
    fixtureDef.restitution = 0.05;
    fixtureDef.friction = 1.0;

	// Delta angle to step by
    this->m_fdeltaAngle = (2.f * M_PI) / NUM_SEGMENTS;

	// Radius of the wheel
    m_fRadius = 75;

	// Need to store the bodies so that we can refer back
    // to it when we connect the joints
    //bodies = [[NSMutableArray alloc] init];

	for (int i = 0; i < NUM_SEGMENTS; i++) 
	{
        // Current angle
        float theta = m_fdeltaAngle*i;
 
        // Calculate x and y based on theta
        float x = m_fRadius*cosf(theta);
        float y = m_fRadius*sinf(theta);
        // Remember to divide by PTM_RATIO to convert to Box2d coordinate
        b2Vec2 circlePosition = b2Vec2(x/PTM_RATIO, y/PTM_RATIO);
 
        b2BodyDef bodyDef;
        bodyDef.type = b2_dynamicBody;
        // Position should be relative to the center
        bodyDef.position = (center + circlePosition);
 
        // Create the body and fixture
        b2Body *body;
        body = world->CreateBody(&bodyDef);
        body->CreateFixture(&fixtureDef);
		
 
        // 將完成初始化的body放到list
		this->bodyList.push_back(body);
    }

	// Circle at the center (inner circle)
    b2BodyDef innerCircleBodyDef;
    // Make the inner circle larger
    innerCircleShape.m_radius = 1.0f;
	fixtureDef.shape = &innerCircleShape;
    innerCircleBodyDef.type = b2_dynamicBody;

	// Position is at the center
    innerCircleBodyDef.position = center;
    innerCircleBody = world->CreateBody(&innerCircleBodyDef);
    innerCircleFix = innerCircleBody->CreateFixture(&fixtureDef);

	// Connect the joints
    b2DistanceJointDef jointDef;
	
	// 元素、跌代器 (類似pointer概念)
	b2Body *curBody = this->bodyList.front(); // 跌帶器(pointer)指向元素, front 是指bodylist的第一個元素, begin 是指第一個跌帶器
	//this->bodyList

	// it != this->bodyList.end() 指向的元素為NULL , 所以 *(this->bodyList.end()) 會導致中斷
	for (list<b2Body*>::iterator it = this->bodyList.begin(); it != this->bodyList.end(); it++)
	{
		b2Body *nextBody;
		if (*it == this->bodyList.back())
			nextBody = this->bodyList.front();
		else
		{
			nextBody = *(++it);
			it--;
		}

		// Connect the outer circles to each other
		jointDef.Initialize(curBody, nextBody,
							curBody->GetWorldCenter(), 
							nextBody->GetWorldCenter() );
		
		// Specifies whether the two connected bodies should collide with each other
		jointDef.collideConnected = true;
		jointDef.frequencyHz = 0.0f;
		jointDef.dampingRatio = 0.0f;
 
		jointList.push_back(world->CreateJoint(&jointDef));
 
		// Connect the center circle with other circles        
		jointDef.Initialize(curBody, innerCircleBody, curBody->GetWorldCenter(), center);
		jointDef.collideConnected = true;
		jointDef.frequencyHz = 8.0;
		jointDef.dampingRatio = 0.5;
 
		jointList.push_back(world->CreateJoint(&jointDef));		
		curBody = nextBody;
	}
}
void SoftBall::bounce()
{
	Expand();
	//Shrink();
	b2Vec2 impulse = b2Vec2(innerCircleBody->GetMass() * 0, innerCircleBody->GetMass() * 10);
    b2Vec2 impulsePoint = innerCircleBody->GetPosition();
    innerCircleBody->ApplyLinearImpulse(impulse, impulsePoint);	
	//innerCircleBody
	
}
void SoftBall::draw()
{
	triangleFanPos[0] = Vertex2DMake(innerCircleBody->GetPosition().x * PTM_RATIO - this->getPositionX(), 
                                     innerCircleBody->GetPosition().y * PTM_RATIO - this->getPositionY());
    // Use each box2d body as a vertex and calculate coordinate for the triangle fan

    for (list<b2Body*>::iterator it = this->bodyList.begin(); it != this->bodyList.end(); it++) 
	{
        b2Body *currentBody = *it;
		currentBody->SetFixedRotation(true);
		Vertex2D pos = Vertex2DMake(currentBody->GetPosition().x * PTM_RATIO , 
                                    currentBody->GetPosition().y * PTM_RATIO );

        int iIndex = std::distance(this->bodyList.begin(), it);
		triangleFanPos[iIndex+1] = Vertex2DMake(pos.x, pos.y);

    }
    // Loop back to close off the triangle fan
    triangleFanPos[NUM_SEGMENTS+1] = triangleFanPos[1];
 
    // The first vertex is the center of the triangle fan.
    // So the first coordinate of the texture map should
    // map to the center of the triangle fan. The texture
    // map coordinates are normalized between 0 and 1, so
    // the center is (0.5, 0.5)
    textCoords[0] = Vertex2DMake(0.5f, 0.5f);

    for (int i = 0; i < NUM_SEGMENTS; i++) 
	{
         GLfloat theta = M_PI + (m_fdeltaAngle * i);
 
        // Calculate the X and Y coordinates for texture mapping.
        // Need to normalize the cosine and sine functions so that the
        // values are between 0 and 1. Cosine and sine functions have
        // values from -1 to 1, so we divide by 2 and add 0.5 to it to
        // normalize the values between 0 and 1.
        textCoords[i+1] = Vertex2DMake(0.5+cosf(theta)*0.5, 
                                       0.5+sinf(theta)*0.5);
    }
	textCoords[NUM_SEGMENTS+1] = textCoords[1];

#ifdef IOS_PLATFORM
    // Enable texture mapping stuff
    glEnable(GL_TEXTURE_2D);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableClientState(GL_COLOR_ARRAY);

	glBindTexture(GL_TEXTURE_2D, this->texture->getName()); // Bind the OpenGL texture
    glTexCoordPointer(2, GL_FLOAT, 0, textCoords); // Send the texture coordinates to OpenGL
    glVertexPointer(2, GL_FLOAT, 0, triangleFanPos); // Send the polygon coordinates to OpenGL
    glDrawArrays(GL_TRIANGLE_FAN, 0, NUM_SEGMENTS+2); // Draw it 
 
    glEnableClientState(GL_COLOR_ARRAY);
#endif

#ifdef WINDOWS_PLATFORM
	
	//ccGLEnable(CC_GL_BLEND);
	ccGLBindTexture2D(texture->getName());
	texture->getShaderProgram()->use();

	//texture->getShaderProgram()->setUniformForModelViewProjectionMatrix();
	texture->getShaderProgram()->setUniformsForBuiltins();

	ccGLEnableVertexAttribs( kCCVertexAttribFlag_Position | kCCVertexAttribFlag_TexCoords);
	ccGLBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );    
    
	glVertexAttribPointer(kCCVertexAttrib_Position, 2, GL_FLOAT, GL_FALSE, 0, triangleFanPos);
    glVertexAttribPointer(kCCVertexAttrib_TexCoords, 2, GL_FLOAT, GL_FALSE, 0, textCoords);

	glDrawArrays(GL_TRIANGLE_FAN, 0, NUM_SEGMENTS+2);
	
#endif
}
void SoftBall::Expand()
{
	float fExpRate=0.1f;
	fExpRate+=1.0f;
	m_fRadius=m_fRadius*fExpRate;
	innerCircleShape.m_radius=innerCircleShape.m_radius*fExpRate;
	BreakAllJoint();
	//innerCircle
	CCLog("radius:%f",m_fRadius);
	b2FixtureDef fixtureDef;
    fixtureDef.shape = &innerCircleShape;
    fixtureDef.density = 0.1;
    fixtureDef.restitution = 0.05;
    fixtureDef.friction = 1.0;
	b2BodyDef innerCircleBodyDef;
    // Make the inner circle larger
    fixtureDef.shape = &innerCircleShape;
    innerCircleBodyDef.type = b2_dynamicBody;

	innerCircleBody->DestroyFixture(innerCircleFix);
    innerCircleFix=innerCircleBody->CreateFixture(&fixtureDef);

	//-------------------------------
	
	//重新生成外圍circle
	
	
	//重綁joint
	b2Vec2 center = innerCircleBody->GetPosition();

	b2CircleShape circleShape;
    circleShape.m_radius = 0.1f;

	fixtureDef.shape = &circleShape;
	list<b2Body*>::iterator it = this->bodyList.begin();
	for (int i = 0; i < NUM_SEGMENTS; i++,it++) 
	{
		b2Body *curBody=(*it);
		// Current angle
        float theta = m_fdeltaAngle*i;
 
        // Calculate x and y based on theta
        float x = m_fRadius*cosf(theta);
        float y = m_fRadius*sinf(theta);
        // Remember to divide by PTM_RATIO to convert to Box2d coordinate
        b2Vec2 circlePosition = b2Vec2(x/PTM_RATIO, y/PTM_RATIO);

		curBody->SetTransform(center+circlePosition,curBody->GetAngle());
	}
	//刪除外圍circle
	/*
	for (list<b2Body*>::iterator it = this->bodyList.begin();it != this->bodyList.end();it++)
	{
		(*it)->DestroyFixture((*it)->GetFixtureList());
	}
	
	bodyList.clear();

	for (int i = 0; i < NUM_SEGMENTS; i++) 
	{
        // Current angle
        float theta = m_fdeltaAngle*i;
 
        // Calculate x and y based on theta
        float x = m_fRadius*cosf(theta);
        float y = m_fRadius*sinf(theta);
        // Remember to divide by PTM_RATIO to convert to Box2d coordinate
        b2Vec2 circlePosition = b2Vec2(x/PTM_RATIO, y/PTM_RATIO);

		
        b2BodyDef bodyDef;
        bodyDef.type = b2_dynamicBody;
        // Position should be relative to the center
        bodyDef.position = (center + circlePosition);
 
        // Create the body and fixture
        b2Body *body;
        body = pWorld->CreateBody(&bodyDef);
        body->CreateFixture(&fixtureDef);
		
 
        // 將完成初始化的body放到list
		this->bodyList.push_back(body);		
    }*/



	// Connect the joints
    b2DistanceJointDef jointDef;
		
	// 元素、跌代器 (類似pointer概念)
	b2Body *curBody = this->bodyList.front(); // 跌帶器(pointer)指向元素, front 是指bodylist的第一個元素, begin 是指第一個跌帶器
	//this->bodyList
	// it != this->bodyList.end() 指向的元素為NULL , 所以 *(this->bodyList.end()) 會導致中斷
	for (list<b2Body*>::iterator it = this->bodyList.begin(); it != this->bodyList.end(); it++)
	{
		b2Body *nextBody;
		if (*it == this->bodyList.back())
			nextBody = this->bodyList.front();
		else
		{
			nextBody = *(++it);
			it--;
		}

		// Connect the outer circles to each other
		jointDef.Initialize(curBody, nextBody,
							curBody->GetWorldCenter(), 
							nextBody->GetWorldCenter() );
		
		// Specifies whether the two connected bodies should collide with each other
		jointDef.collideConnected = true;
		jointDef.frequencyHz = 0.0f;
		jointDef.dampingRatio = 0.0f;
 
		pWorld->CreateJoint(&jointDef);
 
		// Connect the center circle with other circles        
		jointDef.Initialize(curBody, innerCircleBody, curBody->GetWorldCenter(), center);
		jointDef.collideConnected = true;
		jointDef.frequencyHz = 8.0;
		jointDef.dampingRatio = 0.5;
 
		pWorld->CreateJoint(&jointDef);
		curBody = nextBody;
	}
	
}
void SoftBall::Shrink()
{
	float fExpRate=0.05f;
	fExpRate+=1.0f;
	m_fRadius=m_fRadius/fExpRate;
	innerCircleShape.m_radius=innerCircleShape.m_radius/fExpRate;
	BreakAllJoint();
	//innerCircle	
	CCLog("radius:%f",m_fRadius);
	b2FixtureDef fixtureDef;
    fixtureDef.shape = &innerCircleShape;
    fixtureDef.density = 0.1;
    fixtureDef.restitution = 0.05;
    fixtureDef.friction = 1.0;
	b2BodyDef innerCircleBodyDef;
    // Make the inner circle larger
    fixtureDef.shape = &innerCircleShape;
    innerCircleBodyDef.type = b2_dynamicBody;

	innerCircleBody->DestroyFixture(innerCircleFix);
    innerCircleFix=innerCircleBody->CreateFixture(&fixtureDef);

	//-------------------------------
	
	//重新生成外圍circle
	
	
	//重綁joint
	b2Vec2 center = innerCircleBody->GetPosition();

	b2CircleShape circleShape;
    circleShape.m_radius = 0.1f;

	fixtureDef.shape = &circleShape;
	list<b2Body*>::iterator it = this->bodyList.begin();
	for (int i = 0; i < NUM_SEGMENTS; i++,it++) 
	{
		b2Body *curBody=(*it);
		// Current angle
        float theta = m_fdeltaAngle*i;
 
        // Calculate x and y based on theta
        float x = m_fRadius*cosf(theta);
        float y = m_fRadius*sinf(theta);
        // Remember to divide by PTM_RATIO to convert to Box2d coordinate
        b2Vec2 circlePosition = b2Vec2(x/PTM_RATIO, y/PTM_RATIO);

		curBody->SetTransform(center+circlePosition,curBody->GetAngle());
	}
	//刪除外圍circle
	/*
	for (list<b2Body*>::iterator it = this->bodyList.begin();it != this->bodyList.end();it++)
	{
		(*it)->DestroyFixture((*it)->GetFixtureList());
	}
	
	bodyList.clear();

	for (int i = 0; i < NUM_SEGMENTS; i++) 
	{
        // Current angle
        float theta = m_fdeltaAngle*i;
 
        // Calculate x and y based on theta
        float x = m_fRadius*cosf(theta);
        float y = m_fRadius*sinf(theta);
        // Remember to divide by PTM_RATIO to convert to Box2d coordinate
        b2Vec2 circlePosition = b2Vec2(x/PTM_RATIO, y/PTM_RATIO);

		
        b2BodyDef bodyDef;
        bodyDef.type = b2_dynamicBody;
        // Position should be relative to the center
        bodyDef.position = (center + circlePosition);
 
        // Create the body and fixture
        b2Body *body;
        body = pWorld->CreateBody(&bodyDef);
        body->CreateFixture(&fixtureDef);
		
 
        // 將完成初始化的body放到list
		this->bodyList.push_back(body);		
    }*/



	// Connect the joints
    b2DistanceJointDef jointDef;
		
	// 元素、跌代器 (類似pointer概念)
	b2Body *curBody = this->bodyList.front(); // 跌帶器(pointer)指向元素, front 是指bodylist的第一個元素, begin 是指第一個跌帶器
	//this->bodyList
	// it != this->bodyList.end() 指向的元素為NULL , 所以 *(this->bodyList.end()) 會導致中斷
	for (list<b2Body*>::iterator it = this->bodyList.begin(); it != this->bodyList.end(); it++)
	{
		b2Body *nextBody;
		if (*it == this->bodyList.back())
			nextBody = this->bodyList.front();
		else
		{
			nextBody = *(++it);
			it--;
		}

		// Connect the outer circles to each other
		jointDef.Initialize(curBody, nextBody,
							curBody->GetWorldCenter(), 
							nextBody->GetWorldCenter() );
		
		// Specifies whether the two connected bodies should collide with each other
		jointDef.collideConnected = true;
		jointDef.frequencyHz = 0.0f;
		jointDef.dampingRatio = 0.0f;
 
		pWorld->CreateJoint(&jointDef);
 
		// Connect the center circle with other circles        
		jointDef.Initialize(curBody, innerCircleBody, curBody->GetWorldCenter(), center);
		jointDef.collideConnected = true;
		jointDef.frequencyHz = 8.0;
		jointDef.dampingRatio = 0.5;
 
		pWorld->CreateJoint(&jointDef);
		curBody = nextBody;
	}
}
void SoftBall::Shrink(float dt)
{
	float fExpRate=0.1f*dt;
	fExpRate+=1.0f;
	m_fRadius=m_fRadius/fExpRate;
	innerCircleShape.m_radius=innerCircleShape.m_radius/fExpRate;
	BreakAllJoint();
	//innerCircle
		CCLog("radius:%f",m_fRadius);
	b2FixtureDef fixtureDef;
    fixtureDef.shape = &innerCircleShape;
    fixtureDef.density = 0.1;
    fixtureDef.restitution = 0.05;
    fixtureDef.friction = 1.0;
	b2BodyDef innerCircleBodyDef;
    // Make the inner circle larger
    fixtureDef.shape = &innerCircleShape;
    innerCircleBodyDef.type = b2_dynamicBody;

	innerCircleBody->DestroyFixture(innerCircleFix);
    innerCircleFix=innerCircleBody->CreateFixture(&fixtureDef);

	//-------------------------------
	
	//重新生成外圍circle
	
	
	//重綁joint
	b2Vec2 center = innerCircleBody->GetPosition();

	b2CircleShape circleShape;
    circleShape.m_radius = 0.1f;

	fixtureDef.shape = &circleShape;
	list<b2Body*>::iterator it = this->bodyList.begin();
	for (int i = 0; i < NUM_SEGMENTS; i++,it++) 
	{
		b2Body *curBody=(*it);
		// Current angle
        float theta = m_fdeltaAngle*i;
 
        // Calculate x and y based on theta
        float x = m_fRadius*cosf(theta);
        float y = m_fRadius*sinf(theta);
        // Remember to divide by PTM_RATIO to convert to Box2d coordinate
        b2Vec2 circlePosition = b2Vec2(x/PTM_RATIO, y/PTM_RATIO);

		curBody->SetTransform(center+circlePosition,curBody->GetAngle());
	}
	//刪除外圍circle
	/*
	for (list<b2Body*>::iterator it = this->bodyList.begin();it != this->bodyList.end();it++)
	{
		(*it)->DestroyFixture((*it)->GetFixtureList());
	}
	
	bodyList.clear();

	for (int i = 0; i < NUM_SEGMENTS; i++) 
	{
        // Current angle
        float theta = m_fdeltaAngle*i;
 
        // Calculate x and y based on theta
        float x = m_fRadius*cosf(theta);
        float y = m_fRadius*sinf(theta);
        // Remember to divide by PTM_RATIO to convert to Box2d coordinate
        b2Vec2 circlePosition = b2Vec2(x/PTM_RATIO, y/PTM_RATIO);

		
        b2BodyDef bodyDef;
        bodyDef.type = b2_dynamicBody;
        // Position should be relative to the center
        bodyDef.position = (center + circlePosition);
 
        // Create the body and fixture
        b2Body *body;
        body = pWorld->CreateBody(&bodyDef);
        body->CreateFixture(&fixtureDef);
		
 
        // 將完成初始化的body放到list
		this->bodyList.push_back(body);		
    }*/



	// Connect the joints
    b2DistanceJointDef jointDef;
		
	// 元素、跌代器 (類似pointer概念)
	b2Body *curBody = this->bodyList.front(); // 跌帶器(pointer)指向元素, front 是指bodylist的第一個元素, begin 是指第一個跌帶器
	//this->bodyList
	// it != this->bodyList.end() 指向的元素為NULL , 所以 *(this->bodyList.end()) 會導致中斷
	for (list<b2Body*>::iterator it = this->bodyList.begin(); it != this->bodyList.end(); it++)
	{
		b2Body *nextBody;
		if (*it == this->bodyList.back())
			nextBody = this->bodyList.front();
		else
		{
			nextBody = *(++it);
			it--;
		}

		// Connect the outer circles to each other
		jointDef.Initialize(curBody, nextBody,
							curBody->GetWorldCenter(), 
							nextBody->GetWorldCenter() );
		
		// Specifies whether the two connected bodies should collide with each other
		jointDef.collideConnected = true;
		jointDef.frequencyHz = 0.0f;
		jointDef.dampingRatio = 0.0f;
 
		pWorld->CreateJoint(&jointDef);
 
		// Connect the center circle with other circles        
		jointDef.Initialize(curBody, innerCircleBody, curBody->GetWorldCenter(), center);
		jointDef.collideConnected = true;
		jointDef.frequencyHz = 8.0;
		jointDef.dampingRatio = 0.5;
 
		pWorld->CreateJoint(&jointDef);
		curBody = nextBody;
	}
}
void SoftBall::BreakAllJoint()
{
	for (list<b2Joint*>::iterator it = this->jointList.begin(); it != this->jointList.end(); it++) 
	{
		pWorld->DestroyJoint(*it);   
	}
	//jointList.clear();
}