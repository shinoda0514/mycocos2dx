#ifndef __SOFTBALL_H__
#define __SOFTBALL_H__

#include "cocos2d.h"

USING_NS_CC;
using namespace std;
#include "Box2D/Box2D.h"

typedef struct 
{
    GLfloat x;
    GLfloat y;
} Vertex2D;
 
static inline Vertex2D Vertex2DMake(GLfloat inX, GLfloat inY) 
{
    Vertex2D ret;
    ret.x = inX;
    ret.y = inY;
    return ret;
}

#define NUM_SEGMENTS 40

class SoftBall : public CCNode
{
public :
	virtual bool init();

	//World
	b2World* pWorld;

	// Texture
    CCTexture2D *texture;
	
	// Physics bodies
    list<b2Body*> bodyList;
	list<b2Joint*> jointList;
	
    float m_fdeltaAngle;
	float m_fRadius;

	// Center circle
    b2Body *innerCircleBody;
	b2CircleShape innerCircleShape;
	b2Fixture *innerCircleFix;
	
	// Polygon vertices
    Vertex2D triangleFanPos[NUM_SEGMENTS+2];

	// Texture coordinate array
    Vertex2D textCoords[NUM_SEGMENTS+2];

	void createPhysicsObject(b2World* world);
	void createPhysicsObject(b2World* world, float fx, float fy);
	void bounce();
	void draw();
	void Expand();
	void Shrink();
	void Shrink(float dt);
	void BreakAllJoint();
};
#endif // __SOFTBALL_H__