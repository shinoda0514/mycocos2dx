#ifndef __FREEHANDDRAW_H__
#define __FREEHANDDRAW_H__

#include "cocos2d.h"

USING_NS_CC;
using namespace std;
#include "Box2D/Box2D.h"

class FreeHandDraw
{
private :
	// for freehand draw
	CCRenderTexture *m_pCanvas;
	CCSprite *m_pBrush;
	CCPoint previousLocation;
	b2Body* currentPlatformBody;
	CCLayer* m_pLayer;
	b2World* m_pWorld;

public :
	FreeHandDraw();
	~FreeHandDraw();

	bool init(CCLayer &layer, b2World &world);

	// for freehand draw
	void addRectangleBetweenPointsToBody(b2Body *body, CCPoint start, CCPoint end);
    CCRect getBodyRectangle(b2Body* body);
	vector<CCPoint> plataformPoints;

	void touchBegan(CCTouch *pTouch);
	void touchMoved(CCTouch *pTouch);
	void touchEnd();
};
#endif // __FREEHANDDRAW_H__