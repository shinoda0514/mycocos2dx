#ifndef __CMUSHROOM_H__
#define __CMUSHROOM_H__

#include "cocos2d.h"
#include "CTimer.h"

USING_NS_CC;

class CMushroom
{
private:
	cocos2d::CCSprite *m_pSinglePic;
	cocos2d::CCSize  m_PicSize;
	cocos2d::CCPoint m_PicPos;
	cocos2d::CCRect  m_PicRect;
	float m_fDeg;
	bool  m_bRot;
	float m_fTime;
	float m_fTimeStaying;
	bool m_bAppear;

	float m_fScale;	 // 圖片縮放的大小
	bool m_bTouched;	 // 是否被按下
	bool m_bVisible; // 是否顯示


public:
	CMushroom();
	void SetInfo(const char *NormalPic, cocos2d::CCLayer &inlayer, cocos2d::CCPoint inPos);
	void OnFrameMove(CTimer timer);
	int TouchesBegin(cocos2d::CCPoint inPos);
	int TouchesMoved(cocos2d::CCPoint inPos); 
	int TouchesEnded(cocos2d::CCPoint inPos); 
	void SetVisible(bool bVisible);
	bool GetVisible();
};


#endif