#include "CTimer.h"


CTimer::CTimer()
{
	Time=0.0f;
}

void CTimer::SetTimer(float dt)
{
	deltaTime=dt;
	Time+=dt;
}

float CTimer::GetDeltaTime()
{
	return(deltaTime);
}

float CTimer::GetTime()
{
	return(Time);
}