﻿#include "AppMacros.h"
#include "CFirstScene.h"


USING_NS_CC;

CCScene* CFirstScene::scene()
{
    // 'scene' is an autorelease object
	CCScene *scene = CCScene::create();
    if( scene == NULL ) {
		// Scene 開啟失敗
		cocos2d::CCLog("CFirstScene 無法建立");
		return(NULL);
	}
    // Scenelayer' is an autorelease object
    CFirstScene *layer = CFirstScene::create();
	
    // add layer as a child to scene
    scene->addChild(layer,0); // SceneLayer 預設都在 z:0

	return scene;
}

bool CFirstScene::init()
{
	// 1. super init first
    if ( !CCLayer::init() )  { return false; }

	// Set Background Picture. 如果背景圖不會改變, 可以使用以下的方式
    CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
    CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

	// Initialize Background sprite, set the position and add it to this layer.
	this->m_pBackgroundPic = CCSprite::create("MultiScenes/background.png");
	this->m_pBackgroundPic->setPosition(ccp(visibleSize.width	/ 2, visibleSize.height / 2));
	this->addChild(this->m_pBackgroundPic, 0);
	

	// 讀入 plist 定義檔
    //CCSpriteFrameCache *frameCache = CCSpriteFrameCache::sharedSpriteFrameCache();
    // frameCache->addSpriteFramesWithFile("MultiScenes/firstscene.plist");

	// 動態圖檔的置入，The texture will be associated with the created sprite frames.
    CCSpriteFrameCache *frameCache = CCSpriteFrameCache::sharedSpriteFrameCache();
    frameCache->addSpriteFramesWithFile("MultiScenes/firstscene.plist"); 
	frameCache->addSpriteFramesWithFile("MultiScenes/mushroom.plist","MultiScenes/mushroom.png"); 
	//frameCache->addSpriteFramesWithFile("MultiScenes/firstscene.plist"); 

	// 建立存放動畫所需要的圖片個數
	CCArray* animFrames = CCArray::createWithCapacity(8);
	CCSpriteFrame* frame = frameCache->spriteFrameByName("mushroom_2.png");  // 加入第一張圖片
	animFrames->addObject(frame);
	frame = frameCache->spriteFrameByName("mushroom_3.png");
	animFrames->addObject(frame);
	frame = frameCache->spriteFrameByName("mushroom_4.png");
	animFrames->addObject(frame);
	frame = frameCache->spriteFrameByName("mushroom_5.png");
	animFrames->addObject(frame);
	frame = frameCache->spriteFrameByName("mushroom_4.png");
	animFrames->addObject(frame);
	frame = frameCache->spriteFrameByName("mushroom_3.png");
	animFrames->addObject(frame);
	frame = frameCache->spriteFrameByName("mushroom_2.png");
	animFrames->addObject(frame);
	frame = frameCache->spriteFrameByName("mushroom_1.png");
	animFrames->addObject(frame);

	// 產生所需要的撥放控制
	CCAnimation* animation = CCAnimation::createWithSpriteFrames(animFrames,1.0f); // 設定一秒撥放一張
	CCAnimate *animate = CCAnimate::create(animation);	// 產生 animation 所需的 animate 控制
	CCRepeatForever  *repeat = CCRepeatForever::create(animate);	// 設定重複撥放 
//	CCRepeat *repeat = CCRepeat::create(animate,2);	// 設定撥放 2 次, 第二個引數控制次數
	m_fPlaySpeed = 5.0f;
	m_pSpeed = CCSpeed::create(repeat, m_fPlaySpeed); // 設定撥放的速度為 10 倍

	// 為 animPic 產生 CCSprite 的實體，指定第一張圖
	m_pAnimPic = CCSprite::createWithSpriteFrameName("mushroom_1.png");  
	m_pAnimPic->setPosition(ccp(visibleSize.width/2-300, visibleSize.height/2));  

	// cocos2d-X V2.0 之後都是以 CCSpriteBatchNode 來加速
	CCSpriteBatchNode* spriteBatchNode = CCSpriteBatchNode::create("MultiScenes/mushroom.png");
	spriteBatchNode->addChild(m_pAnimPic);
	this->addChild(spriteBatchNode);
	
	m_pAnimPic->setScale(0.5);  // 縮小, 所有的圖都會縮小
	m_pAnimPic->runAction(m_pSpeed);	// 這一行將之前建立的動畫撥放與 m_pAnimPic 連結再一起
	m_pSpeed->retain();  // 佔用資源
	// For aminated images
	m_bPlayAnimate = true;

	// add a label shows "First Scene" 
	CCLabelTTF* pLabel = CCLabelTTF::create("The First Scene", "Arial", TITLE_FONT_SIZE); 
//  CCLabelTTF* pLabel = CCLabelTTF::create("The First Scene", "Arial", 32); 
    // position the label on the center of the screen
    pLabel->setPosition(ccp(origin.x +150,
                            origin.y + visibleSize.height - pLabel->getContentSize().height));
	// add the label as a child to this layer
    this->addChild(pLabel, 0);
	// Changing the string is as expensive as creating a new CCLabelTTF. To obtain better performance use CCLabelBMFont
	
	// 要更有效率的更換顯示的文字, 使用 CCLabelBMFont
	m_cScore[8] = '\0'; // 第八個位置放入 null character
	for(int i = 0 ; i < 8 ; i++ ) m_cScore[i] = 48;
	m_pScoreLabel =  CCLabelBMFont::create( m_cScore, "couriernew.fnt"); 
    m_pScoreLabel->setPosition(ccp( visibleSize.width/2, visibleSize.height - m_pScoreLabel->getContentSize().height));
	m_pScoreLabel->setColor(ccRED);
    this->addChild(m_pScoreLabel, 0);
	m_iScore = 0;  // 設定分數為 0

	// 設定切換場景所需的參數
	m_pNextSceneBtn = new CButton;
	m_pNextSceneBtn->SetButtonInfo("redbtn.png", *this, ccp(950,60));
	m_bSwitchScene = false;

	// 記憶體的釋放
	frameCache->removeSpriteFramesFromFile("MultiScenes/firstscene.plist");
	frameCache->removeSpriteFramesFromFile("MultiScenes/mushroom.plist");

	this->schedule(schedule_selector(CFirstScene::OnFrameMove) );
	this->setTouchEnabled(true);
	return true;
}

void CFirstScene::OnFrameMove(float dt)
{
	if( m_bSwitchScene ) { // 切換到 SecondScene
		// 先將這個 SCENE 的 Update(這裡使用 OnFrameMove, 從 schedule update 中移出)
		this->unschedule(schedule_selector(CFirstScene::OnFrameMove)); 
		// 設定場景切換的特效
		CCTransitionPageTurn *pageTurn = CCTransitionPageTurn::create(1.0F, CSecondScene::scene(), false);
		CCDirector::sharedDirector()->replaceScene(pageTurn);
//		CocosDenshion::SimpleAudioEngine::sharedEngine()->stopBackgroundMusic(true);
	}
	CCLog("e04");
}

void CFirstScene::ccTouchesBegan(CCSet *touches, CCEvent *events)
{
    CCTouch *touch = (CCTouch*)touches->anyObject();
	CCPoint touchLocation = touch->getLocationInView(); // 螢幕座標,左上角是 (0,0) ,座標是整數以 PIXEL 為單位
	CCPoint touchPoint = CCDirector::sharedDirector()->convertToGL(touchLocation); // 轉換到OPENGL 的座標, 右下角是 (0,0) ,座標是浮點數 

	if( m_pNextSceneBtn->TouchesBegin(touchPoint) ) { // 按到就進行場景切換
		m_bSwitchScene = true;
	}
}

void CFirstScene::ccTouchesMoved(CCSet *touches, CCEvent *events)
{
    CCTouch *touch = (CCTouch*)touches->anyObject();
    CCPoint touchLocation = touch->getLocationInView();
	CCPoint touchPoint = CCDirector::sharedDirector()->convertToGL(touchLocation);

	m_pNextSceneBtn->TouchesEnded(touchPoint);
}

void CFirstScene::ccTouchesEnded(CCSet* touches, CCEvent* event)
{
    CCTouch *touch = (CCTouch*)touches->anyObject();
    CCPoint touchLocation = touch->getLocationInView();
	CCPoint touchPoint = CCDirector::sharedDirector()->convertToGL(touchLocation);

	m_pNextSceneBtn->TouchesEnded(touchPoint);


	// 以下為動畫撥放的控制
	if( m_bPlayAnimate ) {
		m_pAnimPic->stopAction(m_pSpeed);
		m_bPlayAnimate = false;
	}
	else {
		m_fPlaySpeed++;  // 增加撥放的速度
		m_pSpeed->setSpeed(m_fPlaySpeed);
		m_pAnimPic->runAction(m_pSpeed);
		m_bPlayAnimate = true;
	}

	// 以下為分數的更新
	m_iScore++;
	int i = m_iScore, j=0;
	while( i > 0 ) {
		m_cScore[7-j] = i%10+48;
		i = i / 10;
		j++;
	}
	m_pScoreLabel->setString(m_cScore,true);
}