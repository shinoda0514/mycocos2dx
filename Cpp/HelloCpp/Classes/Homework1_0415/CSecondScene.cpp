﻿#include "CSecondScene.h"
#include "AppMacros.h"

USING_NS_CC;

CCScene* CSecondScene::scene()
{
    // 'scene' is an autorelease object
	CCScene *scene = CCScene::create();
    if( scene == NULL ) {
		// Scene 開啟失敗
		cocos2d::CCLog("CSecondScene 無法建立");
		return(NULL);
	}
    // Scenelayer' is an autorelease object
    CSecondScene *layer = CSecondScene::create();
	
    // add layer as a child to scene
    scene->addChild(layer,0); // SceneLayer 預設都在 z:0

	return scene;
}

bool CSecondScene::init()
{
	// 1. super init first
    if ( !CCLayer::init() )  { return false; }

	// Set Background Picture. 如果背景圖不會改變, 可以使用以下的方式
    CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
    CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

	// 動態圖檔的置入，The texture will be associated with the created sprite frames.
    CCSpriteFrameCache *frameCache = CCSpriteFrameCache::sharedSpriteFrameCache();
    frameCache->addSpriteFramesWithFile("MultiScenes/secondscene.plist"); 

	// Initialize Background sprite, set the position and add it to this layer.
	this->m_pBackgroundPic = CCSprite::createWithSpriteFrameName("background.png");
	this->m_pBackgroundPic->setPosition(ccp(visibleSize.width	/ 2, visibleSize.height / 2));
	this->addChild(this->m_pBackgroundPic, 0);

	// add a label shows "First Scene" 
	CCLabelTTF* pLabel = CCLabelTTF::create("The Second Scene", "Arial", TITLE_FONT_SIZE); 
//  CCLabelTTF* pLabel = CCLabelTTF::create("The First Scene", "Arial", 32); 
    // position the label on the center of the screen
    pLabel->setPosition(ccp(origin.x +150,
                            origin.y + visibleSize.height - pLabel->getContentSize().height));
	// add the label as a child to this layer
    this->addChild(pLabel, 0);
	// Changing the string is as expensive as creating a new CCLabelTTF. To obtain better performance use CCLabelBMFont

	// 設定切換場景所需的參數
	m_pPrevSceneBtn = new CButton;
	m_pPrevSceneBtn->SetButtonInfo("greenbtn.png", *this, ccp(74,60));
	m_bSwitchScene = false;

	frameCache->removeSpriteFramesFromFile("MultiScenes/secondscene.plist");
	this->schedule( schedule_selector(CSecondScene::OnFrameMove) );
	this->setTouchEnabled(true);
	return true;
}

void CSecondScene::OnFrameMove(float dt)
{
	if( m_bSwitchScene ) { // 切換到 SecondScene
		// 先將這個 SCENE 的 Update(這裡使用 OnFrameMove, 從 schedule update 中移出)
		this->unschedule(schedule_selector(CSecondScene::OnFrameMove)); 
		CCTransitionPageTurn *pageTurn = CCTransitionPageTurn::create(1.0f, CFirstScene::scene(), false); // 設定場景切換的特效
		CCDirector::sharedDirector()->replaceScene(pageTurn);
//		CocosDenshion::SimpleAudioEngine::sharedEngine()->stopBackgroundMusic(true);
	}
}

void CSecondScene::ccTouchesBegan(CCSet *touches, CCEvent *events)
{
    CCTouch *touch = (CCTouch*)touches->anyObject();
	CCPoint touchLocation = touch->getLocationInView(); // 螢幕座標,左上角是 (0,0) ,座標是整數以 PIXEL 為單位
	CCPoint touchPoint = CCDirector::sharedDirector()->convertToGL(touchLocation); // 轉換到OPENGL 的座標, 右下角是 (0,0) ,座標是浮點數 

	if( m_pPrevSceneBtn->TouchesBegin(touchPoint) ) { // 按到就進行場景切換
		m_bSwitchScene = true;
	}
}

void CSecondScene::ccTouchesMoved(CCSet *touches, CCEvent *events)
{
    CCTouch *touch = (CCTouch*)touches->anyObject();
    CCPoint touchLocation = touch->getLocationInView();
	CCPoint touchPoint = CCDirector::sharedDirector()->convertToGL(touchLocation);

	m_pPrevSceneBtn->TouchesEnded(touchPoint);
}

void CSecondScene::ccTouchesEnded(CCSet* touches, CCEvent* event)
{
    CCTouch *touch = (CCTouch*)touches->anyObject();
    CCPoint touchLocation = touch->getLocationInView();
	CCPoint touchPoint = CCDirector::sharedDirector()->convertToGL(touchLocation);

	m_pPrevSceneBtn->TouchesEnded(touchPoint);
}