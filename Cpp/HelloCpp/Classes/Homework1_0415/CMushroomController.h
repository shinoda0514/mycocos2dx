#ifndef __CMUSHROOMCONTROLLER_H__
#define __CMUSHROOMCONTROLLER_H__

#include "cocos2d.h"
#include "CMushroom.h"

USING_NS_CC;

class CMushroomController
{
private:
	CMushroom *m_pMushroom;
	int m_iMushroomNumber;
	
public:
	CMushroomController();
	void init();
	void popMushroom(int index);
	void killMushroom(int index);
	void resetAllMushroom();
	int TouchesBegin(cocos2d::CCPoint inPos);
	int TouchesMoved(cocos2d::CCPoint inPos); 
	int TouchesEnded(cocos2d::CCPoint inPos);  
	void OnFrameMove(CTimer timer);

};


#endif