﻿#ifndef __CFIRST_SCENE_H__
#define __CFIRST_SCENE_H__

#include "cocos2d.h"
#include "CButton.h"
#include "CSecondScene.h"
#define COCOS2D_DEBUG 1

// CFirstScene 的主體為 Layer, 這裡稱為 SceneLayer, 將成看成是 Scene
// 所有的 Layer 則包含在這個 SceneLayer 之下

class CFirstScene : public cocos2d::CCLayer
{
private:
	CCSprite *m_pBackgroundPic;

	bool m_bSwitchScene; // true: 代表場景要切換,  false:不用切換場景
	int  m_iNextScene;   // 將要切換的場景編號

	// For aminated images
	bool m_bPlayAnimate;
public:
	~CFirstScene()	{
		m_pSpeed->release();  // 佔用資源
		delete m_pNextSceneBtn;
		this->removeAllChildren();
		CCTextureCache::sharedTextureCache()->removeUnusedTextures();
	}

	CButton *m_pNextSceneBtn;

	// For aminated images
	cocos2d::CCSprite *m_pAnimPic;
	cocos2d::CCSpeed *m_pSpeed;  // 撥放的控制器
	float m_fPlaySpeed;			// 撥放速度

	// For Scoring
	cocos2d::CCLabelBMFont *m_pScoreLabel;
	int m_iScore;
	char m_cScore[9]; // 必須是字串的形式, 因此結尾必須有 null character, 顯示8個字元, 所以必須有 9 個字元

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
	virtual bool init();  
	static cocos2d::CCScene* scene();

	void OnFrameMove(float dt);

    void ccTouchesEnded(cocos2d::CCSet *touches, cocos2d::CCEvent *events);
    void ccTouchesBegan(cocos2d::CCSet *touches, cocos2d::CCEvent *events);
    void ccTouchesMoved(cocos2d::CCSet *touches, cocos2d::CCEvent *events);

	CREATE_FUNC(CFirstScene);

};


#endif