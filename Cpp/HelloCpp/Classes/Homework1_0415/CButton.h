#ifndef __CBUTTON_H__
#define __CBUTTON_H__

#include "cocos2d.h"

USING_NS_CC;

class CButton
{
private:
	cocos2d::CCSprite *m_pSinglePic;
	cocos2d::CCSize  m_PicSize;
	cocos2d::CCPoint m_PicPos;
	cocos2d::CCRect  m_PicRect;
	float m_fDeg;
	bool  m_bRot;
	float m_fTime;

	float m_fScale;	 // 圖片縮放的大小
	bool m_bTouched;	 // 是否被按下
	bool m_bVisible; // 是否顯示


public:
	void SetButtonInfo(const char *NormalPic, cocos2d::CCLayer &inlayer, cocos2d::CCPoint inPos);
	bool TouchesBegin(cocos2d::CCPoint inPos);
	bool TouchesMoved(cocos2d::CCPoint inPos); 
	bool TouchesEnded(cocos2d::CCPoint inPos); 
	void SetVisible(bool bVisible);

};


#endif