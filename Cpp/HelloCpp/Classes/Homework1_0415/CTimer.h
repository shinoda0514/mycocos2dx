#ifndef __CTIMER_H__
#define __CTIMER_H__

#include "cocos2d.h"

USING_NS_CC;

class CTimer
{
private:
	float deltaTime;
	float Time;
	
public:
	CTimer();
	void SetTimer(float dt);
	float GetDeltaTime();
	float GetTime();

};


#endif