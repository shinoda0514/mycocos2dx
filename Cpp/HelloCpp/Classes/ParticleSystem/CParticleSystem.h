#ifndef __CPARTICLE_SYSTEM_H__
#define __CPARTICLE_SYSTEM_H__

#include "cocos2d.h"
#include "CParticle.h"
#include <list>

USING_NS_CC;
using namespace std;

class CParticleSystem
{
private:
	CParticle* m_pParticles;
	list<CParticle*> m_FreeList;//Double linked list
	list<CParticle*> m_InUsedList;
	int m_iFree;// ノ  Particle 计
	int m_iInUsed;  // タㄏノ Particle 计

	int m_iType;  // ヘ玡北だ笲笆家Α琌贺篈
	CCPoint m_TouchPoint;

	bool m_bAlive;  // だ帝篈
	float m_fTime;

	//瞴
	SoftBall *targetSoftball;

	// box2d physic world
	b2World *m_b2world;
public:
	~CParticleSystem();
	void Init(cocos2d::CCLayer &inlayer,b2World *world);
	void Update(float dt);
	void Emitter(); // 砞﹚だ玻ネ︽家Α籔计秖

	void TouchesEnded(const CCPoint &touchPoint);
	void TouchesBegan(const CCPoint &touchPoint);
	void TouchesMoved(const CCPoint &touchPoint);
	void SetSoftBall(SoftBall *softball);
};

#endif