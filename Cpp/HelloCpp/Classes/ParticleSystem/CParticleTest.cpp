﻿#include "CParticleTest.h"
#include "AppMacros.h"

USING_NS_CC;

CCScene* CParticleTest::scene()
{
	// 'scene' is an autorelease object
	CCScene *scene = CCScene::create();
	if( scene == NULL ) {
		// Scene 開啟失敗
		cocos2d::CCLog("CParticleTest 無法建立");
		return(NULL);
	}  
	// Scenelayer' is an autorelease object
	CParticleTest *layer = CParticleTest::create();

	// add layer as a child to scene
	scene->addChild(layer,0); // SceneLayer 預設都在 z:0

	return scene;
}

bool CParticleTest::init()
{
	// 1. super init first
	if ( !CCLayer::init() )  { return false; }

	// 2. 設定這個 SCENE 所需要的東西
	CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
	CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

	// add a label shows "ParticleSystem"  
	CCLabelTTF* pLabel = CCLabelTTF::create("Particle System", "Arial", 32); 
	// position the label on the center of the screen
	pLabel->setPosition(ccp(origin.x +150,
		origin.y + visibleSize.height - pLabel->getContentSize().height));
	// add the label as a child to this layer
	this->addChild(pLabel, 0);

	// Create a world
	b2Vec2 gravity = b2Vec2(0.0f, -5.0f);
	this->m_b2world = new b2World(gravity);

	// Do we want to let bodies sleep?
	this->m_b2world->SetAllowSleeping(true);
	this->m_b2world->SetContinuousPhysics(true);
	m_ParticleControl.Init(*this,m_b2world);

	this->schedule( schedule_selector(CParticleTest::OnFrameMove) );
	this->setTouchEnabled(true);
	return true;
}

void CParticleTest::OnFrameMove(float dt)
{

	m_ParticleControl.Update(dt);

}

CParticleTest::~CParticleTest(){
	//清除未用到的貼圖
	this->removeAllChildren();
	CCTextureCache::sharedTextureCache()->removeUnusedTextures();
}

void CParticleTest::ccTouchesBegan(CCSet *touches, CCEvent *events)
{
	CCTouch *touch = (CCTouch*)touches->anyObject();
	CCPoint touchLocation = touch->getLocationInView(); // 螢幕座標,左上角是 (0,0) ,座標是整數以 PIXEL 為單位
	CCPoint touchPoint = CCDirector::sharedDirector()->convertToGL(touchLocation); // 轉換到OPENGL 的座標, 右下角是 (0,0) ,座標是浮點數 
	m_ParticleControl.TouchesBegan(touchPoint);

}

void CParticleTest::ccTouchesMoved(CCSet *touches, CCEvent *events)
{
	CCTouch *touch = (CCTouch*)touches->anyObject();
	CCPoint touchLocation = touch->getLocationInView();
	CCPoint touchPoint = CCDirector::sharedDirector()->convertToGL(touchLocation);
	m_ParticleControl.TouchesMoved(touchPoint);

}

void CParticleTest::ccTouchesEnded(CCSet* touches, CCEvent* event)
{
	CCTouch *touch = (CCTouch*)touches->anyObject();
	CCPoint touchLocation = touch->getLocationInView();
	CCPoint touchPoint = CCDirector::sharedDirector()->convertToGL(touchLocation);
}