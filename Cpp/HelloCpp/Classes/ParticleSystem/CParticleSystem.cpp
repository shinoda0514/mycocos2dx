#include "CParticleSystem.h"

#define PARTICLETEXTURE "particles/particletexture.plist"

#define NUMBER_PARTICLES 500 // 預設一次取得 500 個 Particles

USING_NS_CC;

void CParticleSystem::Init(cocos2d::CCLayer &inlayer,b2World *world)
{
	m_b2world=world;

	m_iFree = NUMBER_PARTICLES;
	m_iInUsed = 0;
	m_pParticles = new CParticle[NUMBER_PARTICLES]; // 取得所需要的 particle 空間

	CCSpriteFrameCache *frameCache = CCSpriteFrameCache::sharedSpriteFrameCache();
	frameCache->addSpriteFramesWithFile(PARTICLETEXTURE);

	// 分子運動的型態
	m_iType = ASH;

	for( int i = 0 ; i < NUMBER_PARTICLES ; i++ ) {
		switch(this->m_iType){
		case STAY_FOR_TWOSECONDS:
			m_pParticles[i].SetParticle("spark.png",inlayer);
			m_FreeList.push_front(&m_pParticles[i]);
			break;
		case FREE_FLY:
			m_pParticles[i].SetParticle("bubble.png",inlayer);
			m_FreeList.push_front(&m_pParticles[i]);
			break;
		case EXPLOTION:
			m_pParticles[i].SetParticle("spark.png",inlayer);
			m_FreeList.push_front(&m_pParticles[i]);
			break;
		case RAND_EXPLOTION:
			this-> m_bAlive = false;
			this->m_pParticles[i].SetParticle("spark.png",inlayer);
			m_FreeList.push_front(&m_pParticles[i]);
			break;
		case FOUNTAIN:
			this-> m_bAlive = false;
			this->m_pParticles[i].SetParticle("spark.png",inlayer);
			m_FreeList.push_front(&m_pParticles[i]);
			break;
		case ASH:
			this-> m_bAlive = false;
			this->m_pParticles[i].SetParticle("spark.png",inlayer);
			m_FreeList.push_front(&m_pParticles[i]);
			break;
		default:
			break;
		}
	}

	this->m_fTime = 0;

	//釋放資源 particletexture.plist
	frameCache->sharedSpriteFrameCache()->removeSpriteFramesFromFile(PARTICLETEXTURE);

}

void CParticleSystem::Update(float dt)
{
	list <CParticle *>::iterator it;
	if (this->m_iType == RAND_EXPLOTION){
		if(this-> m_bAlive){
			if(this->m_fTime > 0.5f){
				this->Emitter();
				this->m_fTime -= 0.5f;
			}
			this->m_fTime += dt;
		}
	}
	else if (this->m_iType == FOUNTAIN){
		if(this-> m_bAlive){
			this->Emitter();
		}
	}
	else if (this->m_iType == ASH){
		if(this-> m_bAlive){
			this->Emitter();
		}
	}


	if( m_iInUsed != 0 ) { // 有分子需要更新時
		for( it = m_InUsedList.begin(); it != m_InUsedList.end() ; ) {
			if( (*it)->Update(dt) ) { // 分子生命週期已經到達
				// 將目前這一個節點的內容放回 m_FreeList
				m_FreeList.push_front((*it));
				it = m_InUsedList.erase(it); // 移除目前這一個, 
				m_iFree++; m_iInUsed--;
			}
			else it++;
		}
	}

}

void CParticleSystem::Emitter() // 設定分子產生的起始行為模式
{
	if (this->m_iType == RAND_EXPLOTION){
		CParticle *get;
		if( this->m_iFree >= 300 ) {
			CCPoint Prand;
			Prand.x = rand()%(1024/2);
			Prand.y = rand()%(768/2);
			for (int i = 0; i < 100; i++)
			{
				get = this->m_FreeList.front();
				get->SetBehavior(EXPLOTION);
				get->SetPosition(Prand);
				this->m_FreeList.pop_front();
				this->m_InUsedList.push_front(get);
				this->m_iFree--; this->m_iInUsed++;
			}
		}
	}
	else if (this->m_iType == FOUNTAIN){
		CParticle *get;
		if( this->m_iFree >= 0) {
			get = this->m_FreeList.front();
			get->SetBehavior(FOUNTAIN);
			get->SetPosition(this->m_TouchPoint);
			this->m_FreeList.pop_front();
			this->m_InUsedList.push_front(get);
			this->m_iFree--; this->m_iInUsed++;
		}
	}
	else if (this->m_iType == ASH){
		CParticle *get;
		if( this->m_iFree >= 0) {
			get = this->m_FreeList.front();
			get->SetBehavior(ASH);
			get->SetPosition(this->m_TouchPoint);
			get->AshInit(m_b2world,m_TouchPoint.x,m_TouchPoint.y);
			this->m_FreeList.pop_front();
			this->m_InUsedList.push_front(get);
			this->m_iFree--; this->m_iInUsed++;
		}
	}
	

}

CParticleSystem::~CParticleSystem()
{
	if( m_iInUsed != 0 ) m_InUsedList.clear(); // 移除所有的 NODE
	if( m_iFree != 0 ) m_FreeList.clear();
	delete [] m_pParticles  ; // 釋放所有取得資源


}

void CParticleSystem::TouchesBegan(const cocos2d::CCPoint &touchPoint)
{
	CParticle *get;
switch(m_iType) 
{
	case STAY_FOR_TWOSECONDS:
	// 從 m_FreeList 取得一個分子給放到 m_InUsed
		if( m_iFree != 0 ) {
			get = m_FreeList.front();	               // 從 Free list 取得分子
			get->SetBehavior(STAY_FOR_TWOSECONDS);	// 設定行為
			get->SetPosition(touchPoint);	// 設定位置
			m_FreeList.pop_front();		// 將該分子從 Free list 中移除
			m_InUsedList.push_front(get);	// 加入正在使用中的分子 list 中
			m_iFree--; m_iInUsed++;	              // 增減計數器
		}
		else return;// 沒有分子, 所以就不提供
	break;
	case FREE_FLY:
	// 從 m_FreeList 取得一個分子給放到 m_InUsed
		if( m_iFree != 0 ) {
			get = m_FreeList.front();	               // 從 Free list 取得分子
			get->SetBehavior(FREE_FLY);	// 設定行為
			get->SetPosition(touchPoint);	// 設定位置
			m_FreeList.pop_front();		// 將該分子從 Free list 中移除
			m_InUsedList.push_front(get);	// 加入正在使用中的分子 list 中
			m_iFree--; m_iInUsed++;	              // 增減計數器
		}
		else return;// 沒有分子, 所以就不提供
	break;
	case EXPLOTION:
	// 從 m_FreeList 取得一個分子給放到 m_InUsed
		if(this->m_iFree >= 100 ) {   //If 有300顆分子可以使用
			for (int i = 0; i < 100; i++) {   //一次使用100顆分子
			get = this->m_FreeList.front();
			get->SetBehavior(EXPLOTION);
			get->SetPosition(touchPoint);
			this->m_FreeList.pop_front();
			this->m_InUsedList.push_front(get);
			this->m_iFree--; this->m_iInUsed++;
		}
	}
	else return;// 沒有分子, 所以就不提供
	break;
	case RAND_EXPLOTION :
		// 從 m_FreeList 取得一個分子給放到 m_InUsed
		this-> m_bAlive = !this-> m_bAlive;
		this->m_fTime = 0;
	break;
	case FOUNTAIN:
	    this-> m_bAlive = !this-> m_bAlive;
		this->m_TouchPoint = touchPoint;
	break;
	case ASH:
	    this-> m_bAlive = !this-> m_bAlive;
		this->m_TouchPoint = touchPoint;
		//get->AshInit(m_b2world,touchPoint.x,touchPoint.y);
	break;

}

}

void CParticleSystem::TouchesMoved(const cocos2d::CCPoint &touchPoint)
{
	CParticle *get;
	switch(m_iType) 
	{
	case STAY_FOR_TWOSECONDS:
		// 從 m_FreeList 取得一個分子給放到 m_InUsed
		if( m_iFree != 0 ) {
			get = m_FreeList.front();	               // 從 Free list 取得分子
			get->SetBehavior(STAY_FOR_TWOSECONDS);	// 設定行為
			get->SetPosition(touchPoint);	// 設定位置
			m_FreeList.pop_front();		// 將該分子從 Free list 中移除
			m_InUsedList.push_front(get);	// 加入正在使用中的分子 list 中
			m_iFree--; m_iInUsed++;	              // 增減計數器
		}
		else return;// 沒有分子, 所以就不提供
	break;
	case FREE_FLY:
	// 從 m_FreeList 取得一個分子給放到 m_InUsed
		if( m_iFree != 0 ) {
			get = m_FreeList.front();	               // 從 Free list 取得分子
			get->SetBehavior(FREE_FLY);	// 設定行為
			get->SetPosition(touchPoint);	// 設定位置
			m_FreeList.pop_front();		// 將該分子從 Free list 中移除
			m_InUsedList.push_front(get);	// 加入正在使用中的分子 list 中
			m_iFree--; m_iInUsed++;	              // 增減計數器
		}
		else return;// 沒有分子, 所以就不提供
	break;
	}
	//default
		//break;
}

void CParticleSystem::SetSoftBall(SoftBall *softball)
{
	targetSoftball=softball;
}