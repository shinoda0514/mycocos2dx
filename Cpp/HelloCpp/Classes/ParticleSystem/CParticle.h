#ifndef __CPARTICLE_H__
#define __CPARTICLE_H__

#include "cocos2d.h"
#include "Box2D/Box2D.h"
#include "../Box2D_2/SoftBall.h"
#define   STAY_FOR_TWOSECONDS    1
#define FREE_FLY 2
#define EXPLOTION 3
#define RAND_EXPLOTION 4
#define FOUNTAIN 5
#define ASH 6

USING_NS_CC;

class CParticle
{
private:
	CCSprite *m_Particle;	// 分子本體
	CCPoint m_OldPos;	// 分子前一個位置
	CCPoint m_Pos;		// 分子目前的位置
	CCPoint m_Direction;	// 分子目前的運動方向，單位向量
	float dir;
	float m_fVelocity;		// 分子的速度
	float m_fLifeTime;	// 生命週期
	float m_fMaxEnergy;	// 分子能量的最大值，通常用於改變分子的透明度或亮度
	float m_fEnergy;		// 目前分子的能量，通常用於改變分子的透明度或亮度
	float m_fSpin;		// 分子的旋轉量
	float m_fSize;		// 分子的大小
	ccColor3B m_Color3B;	// 分子的顏色
	bool m_bAlive;		// 分子的活著狀態

	// Constant
	float m_fGravity;// 重力
	float m_fWindSpeed; // 風速

	// 時間
	float m_fTime;// 分子從顯示到目前為止的時間
	float m_fDleayTime;// 分子顯示前的延遲時間

	// 行為模式
	int m_iType;

	// 顯示與否的控制
	bool m_bVisible;

	//Ash
	b2Body *ashPhysicBody;
	//氣球
	SoftBall *targetSoftball;
public:
	bool Update(float dt);
	void SetPosition(const CCPoint &inPos);  // 設定位置
	void SetColor(ccColor3B &color) {m_Color3B = color;} // 設定顏色
	void SetParticle(const char *pngName, cocos2d::CCLayer &inlayer); // 設定分子
	void SetBehavior(int iType); // 設定分子產生的起始行為模式
	void SetGravity(float fg) {m_fGravity = fg;}
	void SetVisible(bool visible);
	void AshInit(b2World *world,float32 fx,float32 fy);
	void AshUpdate();
	void SetSoftBall(SoftBall *softball);

};

#endif