﻿#ifndef __CPARTICLE_TEST_H__
#define __CPARTICLE_TEST_H__

#include "cocos2d.h"
#include "CParticleSystem.h"

// CParticleTest 的主體為 Layer, 這裡稱為 SceneLayer, 將成看成是 Scene
// 所有的 Layer 則包含在這個 SceneLayer 之下

class CParticleTest : public cocos2d::CCLayer
{
private:
	CParticleSystem m_ParticleControl;
	// box2d physic world
	b2World *m_b2world;

public:
	~CParticleTest();
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
	virtual bool init();  
	static cocos2d::CCScene* scene();

	void OnFrameMove(float dt);

    void ccTouchesEnded(cocos2d::CCSet *touches, cocos2d::CCEvent *events);
    void ccTouchesBegan(cocos2d::CCSet *touches, cocos2d::CCEvent *events);
    void ccTouchesMoved(cocos2d::CCSet *touches, cocos2d::CCEvent *events);

	CREATE_FUNC(CParticleTest);
};


#endif