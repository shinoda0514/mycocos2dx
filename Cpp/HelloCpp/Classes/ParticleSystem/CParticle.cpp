#include <cmath>
#include "CParticle.h"

#define TOP_TO_BOTTOM 2.5f
#define MAX_FALLING 768.0f
#define PIXEL_PERM 2.0f*MAX_FALLING/(9.8f*TOP_TO_BOTTOM*TOP_TO_BOTTOM)
#define PTM_RATIO 32.0f

USING_NS_CC;

void CParticle::SetParticle(const char *pngName, cocos2d::CCLayer &inlayer)
{
	m_Particle = CCSprite::createWithSpriteFrameName(pngName);
	m_Particle->setPosition(ccp(rand()%1024, rand()%768));
	m_Particle->setOpacity(255);
	m_Particle->setColor(ccWHITE);
	m_bVisible = false;
	m_Particle->setVisible(false);
	m_iType = 0;
	ccBlendFunc tmp_oBlendFunc = {GL_SRC_ALPHA, GL_ONE};  
	m_Particle->setBlendFunc(tmp_oBlendFunc);

	inlayer.addChild(m_Particle,1);
}


void CParticle::SetBehavior(int iType)
{
	m_iType = iType;   
	switch(m_iType) {     // 根據不同的分子運動形式設定相關的初始參數
	case STAY_FOR_TWOSECONDS:
		m_fVelocity = 0;
		m_fLifeTime = 2;
		m_fMaxEnergy = 1;//最大亮度值
		m_fEnergy = 1;
		m_fGravity = 0;  // 負號代表往下
		m_fSpin = 0;
		m_fSize = 1;
		m_Color3B = ccc3(255,255,255);
		m_bAlive = true;
		m_fTime = 0;
		m_fDleayTime = 0;
	break;
	case FREE_FLY:
		m_fVelocity = 50.0f+(rand()%4000)/1000.0f;
		m_fLifeTime = 2;
		m_fMaxEnergy = 1;//最大亮度值
		m_fEnergy = 1;
		m_fGravity = 0;  // 負號代表往下
		m_fSpin = 0;
		m_fSize = 1;
		m_Color3B = ccc3(55+rand()%200,55+rand()%200,55+rand()%200);
		m_bAlive = true;
		m_fTime = 0;
		m_fDleayTime = 0;
		this->dir = 0;
		this->dir = 100 + 2*M_PI*(rand()%1000)/1000.0f;
		this->m_Direction.x = cosf(dir);
		this->m_Direction.y = sinf(dir);
	break;
	case EXPLOTION:
		//m_fVelocity = 300.0f+(rand()%4000)/1000.0f;
		m_fVelocity = 500.0f;
		//this->m_fVelocity = 10.0f+(rand()%4000)/1000.0f;
		m_fLifeTime = 1.0f;
		m_fMaxEnergy = 1;//最大亮度值
		m_fEnergy = 1;
		m_fGravity = 0;  // 負號代表往下
		m_fSpin = 0;
		m_fSize = 1;
		m_Color3B = ccc3(55+rand()%200,55+rand()%200,55+rand()%200);
		m_bAlive = true;
		m_fTime = 0;
		m_fDleayTime = rand()%101 / 1000.0f;    //煙火出現時間點不同
		this->dir = 0;
		this->dir = 2*M_PI*(rand()%1000)/1000.0f;
		this->m_Direction.x = cosf(dir)*0.5f;
		this->m_Direction.y = sinf(dir)*0.5f;
		//this->m_Direction.x = ( 16*sinf(dir)*sinf(dir)*sinf(dir) )*0.5f;
		//this->m_Direction.y = ( 13*cosf(dir)-5*cosf(2*dir)-2*cosf(3*dir)-cosf(4*dir) )*0.5f;
		this->m_fGravity = -9.8f;
	break;
	case FOUNTAIN:
		m_fVelocity = 200.0f+(rand()%4000)/1000.0f;
		m_fLifeTime = 4.0f;
		m_fMaxEnergy = 1;//最大亮度值
		m_fEnergy = 1;
		m_fGravity = 0;  // 負號代表往下
		m_fSpin = 0;
		m_fSize = 1;
		m_Color3B = ccc3(255,255,255);
		m_bAlive = true;
		m_fTime = 0;
		m_fDleayTime = rand()%101 / 1000.0f;    //煙火出現時間點不同
		this->dir = 0;
		this->dir = this->dir = (M_PI*(rand()%1000)/1000.0f)/5 + M_PI*4/10;
		this->m_Direction.x = cosf(dir);
		this->m_Direction.y = sinf(dir);
		this->m_fGravity = -9.8f;
	break;
	case ASH:
		m_fVelocity = 200.0f+(rand()%4000)/1000.0f;
		m_fLifeTime = 4.0f;
		m_fMaxEnergy = 1;//最大亮度值
		m_fEnergy = 1;
		m_fGravity = 0;  // 負號代表往下
		m_fSpin = 0;
		m_fSize = 1;
		m_Color3B = ccc3(255,255,255);
		m_bAlive = true;
		m_fTime = 0;
		m_fDleayTime = rand()%101 / 1000.0f;    //煙火出現時間點不同
		this->dir = 0;
		this->dir = this->dir = (M_PI*(rand()%1000)/1000.0f)/5 + M_PI*4/10;
		this->m_Direction.x = cosf(dir);
		this->m_Direction.y = sinf(dir);
		this->m_fGravity = -9.8f;
	break;
	}
}

bool CParticle::Update(float dt)
{
	switch(m_iType) {
	case STAY_FOR_TWOSECONDS:
		if(!this->m_bVisible && this->m_fTime>this->m_fDleayTime){            // Delay time
			this->m_fTime = this->m_fTime - this->m_fDleayTime;
			this->m_bVisible = true;
			this->m_Particle->setVisible(this->m_bVisible);
			this->m_Particle->setColor(this->m_Color3B);
			this->m_Particle->setPosition(this->m_Pos);
		}
		else if(m_fTime > m_fLifeTime){                 //生命週期結束
			this->m_bVisible = false;
			this->m_Particle->setVisible(this->m_bVisible);
			return true;
		}
		else if(this->m_bVisible){                  //一般狀況
			float t;
			t = sinf(M_PI*this->m_fTime/this->m_fLifeTime);
			this->m_fEnergy  = this->m_fMaxEnergy*t;
			this->m_Particle->setScale(1+(1-t)*4);
			this->m_Particle->setColor(this->m_Color3B);
			this->m_Particle->setPosition(this->m_Pos);
		}
	break;
	case FREE_FLY:
		if(!this->m_bVisible && this->m_fTime>this->m_fDleayTime){            // Delay time
			this->m_fTime = this->m_fTime - this->m_fDleayTime;
			this->m_bVisible = true;
			this->m_Particle->setVisible(this->m_bVisible);
			this->m_Particle->setOpacity(0);
			this->m_Particle->setColor(this->m_Color3B);
			this->m_Particle->setPosition(this->m_Pos);
		}
		else if(m_fTime > m_fLifeTime){                 //生命週期結束
			this->m_bVisible = false;
			this->m_Particle->setVisible(this->m_bVisible);this->m_Particle->setOpacity(0);
			return true;
		}
		else if(this->m_bVisible){                  //一般狀況
			float t;
			t = sinf(M_PI*this->m_fTime/this->m_fLifeTime);
			this->m_fEnergy  = this->m_fMaxEnergy*t;
			this->m_Particle->setScale(1+(1-t)*0.5f);
			this->m_Particle->setOpacity(225*t);
			this->m_Particle->setColor(this->m_Color3B);
			this->m_Pos.x +=m_Direction.x*m_fVelocity*dt;
			this->m_Pos.y +=m_Direction.y*m_fVelocity*dt;
			this->m_Particle->setPosition(this->m_Pos);
		}
	break;
	case EXPLOTION:
		if(!this->m_bVisible && this->m_fTime>this->m_fDleayTime){            // Delay time
			this->m_fTime = this->m_fTime - this->m_fDleayTime;
			this->m_bVisible = true;
			this->m_Particle->setVisible(this->m_bVisible);
			this->m_Particle->setOpacity(0);
			this->m_Particle->setColor(this->m_Color3B);
			this->m_Particle->setPosition(this->m_Pos);
		}
		else if(m_fTime > m_fLifeTime){                 //生命週期結束
			this->m_bVisible = false;
			this->m_Particle->setVisible(this->m_bVisible);this->m_Particle->setOpacity(0);
			return true;
		}
		else if(this->m_bVisible){                  //一般狀況
			float t;
			t = sinf(M_PI*this->m_fTime/this->m_fLifeTime);
			this->m_fEnergy  = this->m_fMaxEnergy*t;
			this->m_Particle->setScale(1+(1-t)*0.5f);
			this->m_Particle->setOpacity(225*t);
			this->m_Particle->setColor(this->m_Color3B);
			this->m_Pos.x +=m_Direction.x*m_fVelocity*dt*t;
			this->m_Pos.y +=m_Direction.y*m_fVelocity*dt*t;
			//this->m_Particle->setPosition(this->m_Pos);
			t = this->m_fTime +dt;
 			this->m_Pos.y += (PIXEL_PERM*0.5f*m_fGravity*(t*t-m_fTime*m_fTime));     //S = g*t*t/2
			this->m_Particle->setPosition(this->m_Pos);
		}
	break;
	case FOUNTAIN:
		if(!this->m_bVisible && this->m_fTime>this->m_fDleayTime){            // Delay time
			this->m_fTime = this->m_fTime - this->m_fDleayTime;
			this->m_bVisible = true;
			this->m_Particle->setVisible(this->m_bVisible);
			this->m_Particle->setColor(this->m_Color3B);
			this->m_Particle->setPosition(this->m_Pos);
		}
		else if(m_fTime > m_fLifeTime){                 //生命週期結束
			this->m_bVisible = false;
			this->m_Particle->setVisible(this->m_bVisible);
			return true;
		}
		else if(this->m_bVisible){                  //一般狀況
			float t;
			t = sinf(M_PI*this->m_fTime/this->m_fLifeTime);
			this->m_fEnergy  = this->m_fMaxEnergy*t;
			this->m_Particle->setScale(1+(1-t)*0.5f);
			this->m_Particle->setColor(this->m_Color3B);
			this->m_Pos.x +=m_Direction.x*m_fVelocity*dt;
			this->m_Pos.y +=m_Direction.y*m_fVelocity*dt;
			t = this->m_fTime +dt;
			this->m_Pos.y += (PIXEL_PERM*0.5f*m_fGravity*(t*t-m_fTime*m_fTime));     //S = g*t*t/2
			this->m_Particle->setPosition(this->m_Pos);
		}
	break;
	case ASH:
		AshUpdate();
	break;
	}
m_fTime+=dt;
return false;
}

void CParticle::SetVisible(bool bVisible)
{
	this->m_bVisible = bVisible;
	this->m_Particle->setVisible(m_bVisible);
}

void CParticle::SetPosition(const cocos2d::CCPoint &inPos) {
	this->m_Pos = inPos;
};

void CParticle::AshInit(b2World *world,float32 fx,float32 fy)
{
	// Center is the position of the circle that is in the center (inner circle)
    b2Vec2 center = b2Vec2(fx/PTM_RATIO, fy/PTM_RATIO);

	b2CircleShape circleShape;
    circleShape.m_radius = 0.1f;

	b2FixtureDef fixtureDef;
    fixtureDef.shape = &circleShape;
    fixtureDef.density = 0.1;
    fixtureDef.restitution = 0.05;
    fixtureDef.friction = 1.0;

	
    b2BodyDef bodyDef;
    bodyDef.type = b2_dynamicBody;
    // Position should be relative to the center
    bodyDef.position = center;
 
	// Create the body and fixture
    ashPhysicBody = world->CreateBody(&bodyDef);
    ashPhysicBody->CreateFixture(&fixtureDef);
	    
}
void CParticle::SetSoftBall(SoftBall *softball)
{
	targetSoftball=softball;
}
void CParticle::AshUpdate()
{
	this->m_bVisible = true;
	this->m_Particle->setVisible(this->m_bVisible);
	this->m_Particle->setOpacity(0);
	this->m_Particle->setColor(this->m_Color3B);
	this->m_Particle->setPosition(this->m_Pos);

	this->m_Pos.x=ashPhysicBody->GetPosition().x * PTM_RATIO;
	this->m_Pos.y=ashPhysicBody->GetPosition().y * PTM_RATIO;
	/*
	float disX=targetSoftball->getPosition().x-ashPhysicBody->GetPosition().x;
	float disY=targetSoftball->getPosition().y-ashPhysicBody->GetPosition().y;
	
	if((disX*disX+disY*disY) 
		<(targetSoftball->m_fRadius*1.1*targetSoftball->m_fRadius*1.1))
	{
		disX=disX/sqrtf((disX*disX+disY*disY));
		disY=disY/sqrtf((disX*disX+disY*disY));
		b2Vec2 impulse = b2Vec2(disX*ashPhysicBody->GetMass() * 10, disY*ashPhysicBody->GetMass() * 10);
		b2Vec2 impulsePoint = ashPhysicBody->GetPosition();
		ashPhysicBody->ApplyLinearImpulse(impulse, impulsePoint);	
	}*/
}