﻿#ifndef __CFIRST_SCENE_H__
#define __CFIRST_SCENE_H__

#include "cocos2d.h"

USING_NS_CC;

// CScene101 的主體為 Layer, 這裡稱為 SceneLayer, 將其看成是 Scene
// 所有的 Layer 則包含在這個 SceneLayer 之下

class CScene101 : public cocos2d::CCLayer
{
private:
	CCSprite *background;

public:

	~CScene101()	{ // 解構元

	}
	CCSprite *pSinglePic;

	// for Button
	CCSprite *m_pButton;
	CCSize  m_ButtonSize;
	CCPoint m_ButtonPos;
	CCRect  m_ButtonRect;

	// For Scoring
	CCLabelBMFont *m_pScoreLabel;
	int m_iScore;
	char m_cScore[9]; // 必須是字串的形式, 因此結尾必須有 null character, 顯示8個字元, 所以必須有 9 個字元

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
	virtual bool init();  

	static cocos2d::CCScene* scene();

	void OnFrameMove(float dt);

    void ccTouchesEnded(CCSet *pTouches, CCEvent *pEvents);
    void ccTouchesBegan(CCSet *pTouches, CCEvent *pEvents);
    void ccTouchesMoved(CCSet *pTouches, CCEvent *pEvents);

	CREATE_FUNC(CScene101);	
};


#endif