﻿#include "CScene101.h"
#include "AppMacros.h"

USING_NS_CC;

CCScene* CScene101::scene()
{
    // 'scene' is an autorelease object
	CCScene *scene = CCScene::create();
    if( scene == NULL ) {
		// Scene 開啟失敗
		cocos2d::CCLog("CScene101 無法建立");
		return(NULL);
	}
    // Scenelayer' is an autorelease object
    CScene101 *layer = CScene101::create();
	
    // add layer as a child to scene
    scene->addChild(layer, 0); // SceneLayer 預設都在 z:0

	return scene;
}

bool CScene101::init()
{
	// 1. super init first
    if ( !CCLayer::init() )  { return false; }

	// 2. 設定這個 SCENE 所需要的東西
	// 取得開啟的視窗大小與原點的位置
    CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
    CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();
	CCSpriteFrameCache *frameCache = CCSpriteFrameCache::sharedSpriteFrameCache();
	frameCache->addSpriteFramesWithFile("scene101/pic.plist");


	// Initialize Background sprite, set the position and add it to this layer.
	this->background = CCSprite::createWithSpriteFrameName("background.png");
	this->background->setPosition(ccp(visibleSize.width	/ 2, visibleSize.height / 2));
	this->addChild(this->background, 0);

	// 單一圖檔透過 CCSprite 置入
	pSinglePic = CCSprite::createWithSpriteFrameName("etmouse.png");
	pSinglePic->setPosition(ccp(visibleSize.width	/ 2, visibleSize.height / 2)); // 設定位置
	pSinglePic->setColor(ccWHITE);  // pSinglePic->setColor(ccc3(128,0,255));
	pSinglePic->setRotation(45);    // 順時針方向旋轉, pSinglePic->setRotationY(90);
	pSinglePic->setScale(1.25);		// 縮放
	pSinglePic->setOpacity(255);		// 透明度 0 ~ 255(完全不透明)
	this->addChild(pSinglePic,1);   // 加入目前的 Layer 中 	1: Z 軸的層次，越大代表在越上層

	// 單一圖檔透過 CCSprite 置入
	CCSprite *m_pButton = CCSprite::createWithSpriteFrameName("etmouse.png");
	m_pButton->setPosition(ccp(visibleSize.width	/ 4, visibleSize.height / 4)); // 設定位置
	m_pButton->setColor(ccWHITE);  // pSinglePic->setColor(ccc3(128,0,255));
	m_pButton->setRotation(45);    // 順時針方向旋轉, pSinglePic->setRotationY(90);
	m_pButton->setScale(1.25);		// 縮放
	m_pButton->setOpacity(255);		// 透明度 0 ~ 255(完全不透明)
	this->addChild(m_pButton,1);   // 加入目前的 Layer 中 	1: Z 軸的層次，越大代表在越上層

	m_ButtonSize = m_pButton->getContentSize();
	m_ButtonPos = m_pButton->getPosition();

	// 設定判斷區域
	m_ButtonRect.size = m_ButtonSize;
	m_ButtonRect.origin.x = m_ButtonPos.x - m_ButtonSize.width*0.5f;
	m_ButtonRect.origin.y = m_ButtonPos.y - m_ButtonSize.height*0.5f;



	// add a label shows "First Scene" 
//  CCLabelTTF* pLabel = CCLabelTTF::create("The First Scene", "Arial", TITLE_FONT_SIZE); 
    CCLabelTTF* pLabel = CCLabelTTF::create("The First Scene", "Arial", 32); 
    // position the label on the center of the screen
    pLabel->setPosition(ccp(origin.x +150,
                            origin.y + visibleSize.height - pLabel->getContentSize().height));
	pLabel->setColor(ccc3(255,0,0));
	// add the label as a child to this layer
    this->addChild(pLabel, 0);
	// Changing the string is as expensive as creating a new CCLabelTTF. To obtain better performance use CCLabelBMFont
	
	// 要更有效率的更換顯示的文字, 使用 CCLabelBMFont
	m_cScore[8] = '\0'; // 第八個位置放入 null character
	for(int i = 0 ; i < 8 ; i++ ) m_cScore[i] = 48;
	m_pScoreLabel =  CCLabelBMFont::create( m_cScore, "couriernew.fnt"); 
    m_pScoreLabel->setPosition(ccp( visibleSize.width/2, visibleSize.height - m_pScoreLabel->getContentSize().height));
	m_pScoreLabel->setColor(ccRED);
    this->addChild(m_pScoreLabel, 0);
	m_iScore = 0;  // 設定分數為 0

	this->schedule( schedule_selector(CScene101::OnFrameMove) );
	this->setTouchEnabled(true);
	return true;
}

void CScene101::OnFrameMove(float dt)
{


}

void CScene101::ccTouchesBegan(CCSet *touches, CCEvent *events)
{
    CCTouch *touch = (CCTouch*)touches->anyObject();
	CCPoint touchLocation = touch->getLocationInView(); // 螢幕座標,左上角是 (0,0) ,座標是整數以 PIXEL 為單位
	CCPoint touchPoint = CCDirector::sharedDirector()->convertToGL(touchLocation); // 轉換到OPENGL 的座標, 左下角是 (0,0) ,座標是浮點數 

	if( m_ButtonRect.containsPoint(touchPoint) ) 
	{
		if(pSinglePic->isVisible())pSinglePic->setVisible(false);
		else pSinglePic->setVisible(true);
	}

}

void CScene101::ccTouchesMoved(CCSet *touches, CCEvent *events)
{
    CCTouch *touch = (CCTouch*)touches->anyObject();
    CCPoint touchLocation = touch->getLocationInView();
	CCPoint touchPoint = CCDirector::sharedDirector()->convertToGL(touchLocation);

}

void CScene101::ccTouchesEnded(CCSet* touches, CCEvent* event)
{
    CCTouch *touch = (CCTouch*)touches->anyObject();
    CCPoint touchLocation = touch->getLocationInView();
	CCPoint touchPoint = CCDirector::sharedDirector()->convertToGL(touchLocation);


	// 以下為分數的更新
	m_iScore++;
	int i = m_iScore, j=0;
	while( i > 0 ) {
		m_cScore[7-j] = i%10+48;
		i = i / 10;
		j++;
	}
	m_pScoreLabel->setString(m_cScore,true);
}