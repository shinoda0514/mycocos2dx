#include "CMushroomController.h"

CMushroomController::CMushroomController()
{
	
}


void CMushroomController::init(cocos2d::CCLayer &inlayer)
{
	CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
	m_pMushroom=new CMushroom[9];
	for(int i=0;i<9;i++)
	{

		m_pMushroom[i].SetInfo("mushroom_3.png", inlayer, ccp(visibleSize.width*(1+i%3)/4 ,visibleSize.height*(1+i/3)/4));
		m_pMushroom[i].setVisible(false);
	}
}

void CMushroomController::OnFrameMove(CTimer &timer)
{
	if(rand()%100<2  &&  ((int)timer.getTime())%2==0)
	{
		int index=rand()%9;
		popMushroom(index);
		
	}	

	for(int i=0;i<9;i++)
	{
		m_pMushroom[i].OnFrameMove(timer);
	}
}

void CMushroomController::popMushroom(int index)
{
	if(!m_pMushroom[index].getVisible())m_pMushroom[index].pop();
}

void CMushroomController::killMushroom(int index)
{
	m_pMushroom[index].kill();
}

int CMushroomController::TouchesBegin(cocos2d::CCPoint inPos)
{
	int score=0;
	for(int i=0;i<9;i++)
	{
		score+=m_pMushroom[i].TouchesBegin(inPos);
	}
	return(score);
}

int CMushroomController::TouchesMoved(cocos2d::CCPoint inPos)
{
	int score=0;
	for(int i=0;i<9;i++)
	{
		score+=m_pMushroom[i].TouchesMoved(inPos);
	}
	return(score);
}

int CMushroomController::TouchesEnded(cocos2d::CCPoint inPos)
{
	int score=0;
	for(int i=0;i<9;i++)
	{
		score+=m_pMushroom[i].TouchesEnded(inPos);
	}
	return(score);
}