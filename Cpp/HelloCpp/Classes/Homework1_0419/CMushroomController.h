#ifndef __CMUSHROOMCONTROLLER_H__
#define __CMUSHROOMCONTROLLER_H__

#include "cocos2d.h"
#include "CMushroom.h"

USING_NS_CC;

class CMushroomController
{
private:
	CMushroom *m_pMushroom;
	
public:
	CMushroomController();
	void init(cocos2d::CCLayer &inlayer);
	void OnFrameMove(CTimer &timer);
	void popMushroom(int index);
	void killMushroom(int index);
	int TouchesBegin(cocos2d::CCPoint inPos);
	int TouchesMoved(cocos2d::CCPoint inPos); 
	int TouchesEnded(cocos2d::CCPoint inPos); 


};


#endif