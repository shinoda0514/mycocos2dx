#include "CButton.h"


void CButton::SetButtonInfo(const char *NormalPic, cocos2d::CCLayer &inlayer, cocos2d::CCPoint inPos)
{
	m_pSinglePic = CCSprite::createWithSpriteFrameName(NormalPic);
	m_PicPos = inPos;
	m_pSinglePic->setPosition(m_PicPos); // 設定位置
	m_pSinglePic->setColor(ccWHITE);  // m_pSinglePic->setColor(ccc3(128,0,255));
	m_fDeg = 0; m_fTime = 0;
	m_pSinglePic->setRotation(m_fDeg);   // 順時針方向旋轉, m_pSinglePic->setRotationY(90);
	m_pSinglePic->setScale(1.0);   // 縮放
	m_pSinglePic->setOpacity(255);  // 透明度 0 ~ 255(完全不透明)
	inlayer.addChild(m_pSinglePic,1); // 加入目前的 Layer 中 1: Z 軸的層次，越大代表在越上層
	m_bRot = false;

	// 取得大小
	m_PicSize = m_pSinglePic->getContentSize();

	// 設定判斷區域
	m_PicRect.size = m_PicSize;
	m_PicRect.origin.x = m_PicPos.x - m_PicSize.width*0.5f;
	m_PicRect.origin.y = m_PicPos.y - m_PicSize.height*0.5f;

	m_bTouched = false;
}

bool CButton::TouchesBegin(cocos2d::CCPoint inPos)
{
	if( m_PicRect.containsPoint(inPos) ) 
	{
		m_bTouched = true;
		m_pSinglePic->setScale(1.25f);   // 縮放
		return(true); // 有按在上面
	}
	return(false);
}

bool CButton::TouchesMoved(cocos2d::CCPoint inPos)
{
	if( m_bTouched ) { // 只有被按住的時候才處理
		if( !m_PicRect.containsPoint(inPos) ) { // 手指頭位置離開按鈕
			m_bTouched = false;
			m_pSinglePic->setScale(1.0f);   // 縮放
			return(false);
		}
		else return(true);
	}
	return(false); // 是後移到按鈕上將被忽略
}

bool CButton::TouchesEnded(cocos2d::CCPoint inPos)
{
	if( m_bTouched )
	{
		m_bTouched = false;
		m_pSinglePic->setScale(1.0f);   // 縮放
		if( m_PicRect.containsPoint(inPos) )  return(true);  // 手指頭位置按鈕時，還在該按鈕上
	}
	return false;
}

void CButton::SetVisible(bool bVisible)
{

}
