#include "CTimer.h"


CTimer::CTimer(cocos2d::CCLayer &inlayer)
{
	Time=0.0f;
	CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
	
	m_cTime[3]='\n';

	for(int i = 0 ; i < 3 ; i++ ) m_cTime[i] = 48;
	m_pTimeLabel =  CCLabelBMFont::create( m_cTime, "couriernew.fnt"); 
	m_pTimeLabel->setPosition(ccp( visibleSize.width-m_pTimeLabel->getContentSize().width, visibleSize.height - 100-m_pTimeLabel->getContentSize().height));
	m_pTimeLabel->setColor(ccRED);
    inlayer.addChild(m_pTimeLabel, 0);
}

void CTimer::setTimer(float dt)
{
	deltaTime=dt;
	Time+=dt;
}

float CTimer::getDeltaTime()
{
	return(deltaTime);
}

float CTimer::getTime()
{
	return(Time);
}

void CTimer::showTime()
{
	int i = Time, j=0;
	while( i > 0 ) {
		m_cTime[3-j] = i%10+48;
		i = i / 10;
		j++;
	}
	m_pTimeLabel->setString(m_cTime,true);
}