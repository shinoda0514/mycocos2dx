﻿#ifndef __CSECOND_SECNE_H__
#define __CSECOND_SECNE_H__

#include "cocos2d.h"
#include "CButton.h"
#include "CFirstScene.h"

// CFirstScene 的主體為 Layer, 這裡稱為 SceneLayer, 將成看成是 Scene
// 所有的 Layer 則包含在這個 SceneLayer 之下

class CSecondScene : public cocos2d::CCLayer
{
private:
	CCSprite *m_pBackgroundPic;

	bool m_bSwitchScene; // true: 代表場景要切換,  false:不用切換場景

	// For aminated images
	bool m_bPlayAnimate;
public:
	~CSecondScene()	{
		delete m_pPrevSceneBtn;
		this->removeAllChildren();
		CCTextureCache::sharedTextureCache()->removeUnusedTextures();
	}

	CButton *m_pPrevSceneBtn;

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
	virtual bool init();  
	static cocos2d::CCScene* scene();

	void OnFrameMove(float dt);

    void ccTouchesEnded(cocos2d::CCSet *touches, cocos2d::CCEvent *events);
    void ccTouchesBegan(cocos2d::CCSet *touches, cocos2d::CCEvent *events);
    void ccTouchesMoved(cocos2d::CCSet *touches, cocos2d::CCEvent *events);

	CREATE_FUNC(CSecondScene);

};


#endif