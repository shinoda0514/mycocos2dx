#include "CScoreCounter.h"

 
CScoreCounter::CScoreCounter(cocos2d::CCLayer &inlayer)
{
	CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
	m_iScore=0;
	m_cScore[8]='\n';

	for(int i = 0 ; i < 8 ; i++ ) m_cScore[i] = 48;
	m_pScoreLabel =  CCLabelBMFont::create( m_cScore, "couriernew.fnt"); 
    m_pScoreLabel->setPosition(ccp( visibleSize.width/2, visibleSize.height - m_pScoreLabel->getContentSize().height));
	m_pScoreLabel->setColor(ccRED);
    inlayer.addChild(m_pScoreLabel, 0);
}

void CScoreCounter::setMushroomController(CMushroomController *mushroomController)
{
	m_pMushroomController=mushroomController;
}

void CScoreCounter::showScore()
{
	CCLog("Score:%d",m_iScore);

	int i = m_iScore, j=0;
	if(i==0)

	{
		for(int ii=0;ii<8;ii++)m_cScore[ii]=48;

	}
	while( i > 0 ) {
		m_cScore[7-j] = i%10+48;
		i = i / 10;
		j++;
	}

	m_pScoreLabel->setString(m_cScore,true);
}

void CScoreCounter::TouchesBegin(cocos2d::CCPoint inPos)
{
	m_iScore+=m_pMushroomController->TouchesBegin(inPos);
	if(m_iScore<1)m_iScore=0;
}

void CScoreCounter::TouchesMoved(cocos2d::CCPoint inPos)
{
	m_pMushroomController->TouchesMoved(inPos);
}

void CScoreCounter::TouchesEnded(cocos2d::CCPoint inPos)
{
	m_pMushroomController->TouchesEnded(inPos);
}

int CScoreCounter::getScore()
{	
	return(m_iScore);
}
