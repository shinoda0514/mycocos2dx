﻿#ifndef __CHOMEWORK_SCENE_H__
#define __CHOMEWORK_SCENE_H__

#include "cocos2d.h"
#include "CButton.h"
#include "CSecondScene.h"
#include "CMushroomController.h"
#include "CTimer.h"
#include "CScoreCounter.h"
//#define COCOS2D_DEBUG 1

// CHomeworkScene 的主體為 Layer, 這裡稱為 SceneLayer, 將成看成是 Scene
// 所有的 Layer 則包含在這個 SceneLayer 之下

class CHomeworkScene : public cocos2d::CCLayer
{
private:
	CCSprite *m_pBackgroundPic;

	//test
	CMushroomController *m_pMushroomController;
	CTimer *m_pTimer;
	CScoreCounter *m_pScoreCounter;
	CCLabelTTF* m_pGameOverLabel;
	//test

	bool m_bSwitchScene; // true: 代表場景要切換,  false:不用切換場景
	bool m_bPlaying;
	bool m_bIsGameOver;

	
public:
	~CHomeworkScene()	{
		delete m_pRestartBtn;
		this->removeAllChildren();
		CCTextureCache::sharedTextureCache()->removeUnusedTextures();
	}

	CButton *m_pRestartBtn;
	CButton *m_pPauseBtn;
	
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
	virtual bool init();  
	static cocos2d::CCScene* scene();

	void OnFrameMove(float dt);

    void ccTouchesEnded(cocos2d::CCSet *touches, cocos2d::CCEvent *events);
    void ccTouchesBegan(cocos2d::CCSet *touches, cocos2d::CCEvent *events);
    void ccTouchesMoved(cocos2d::CCSet *touches, cocos2d::CCEvent *events);

	CREATE_FUNC(CHomeworkScene);

};


#endif