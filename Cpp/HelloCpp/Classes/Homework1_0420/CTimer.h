#ifndef __CTIMER_H__
#define __CTIMER_H__

#include "cocos2d.h"

USING_NS_CC;

class CTimer
{
private:
	float deltaTime;
	float Time;
	cocos2d::CCLabelBMFont *m_pTimeLabel;
	char m_cTime[4]; // 必須是字串的形式, 因此結尾必須有 null character, 顯示8個字元, 所以必須有 9 個字元
	
public:
	CTimer(cocos2d::CCLayer &inlayer);
	void setTimer(float dt);
	float getDeltaTime();
	float getTime();
	void showTime();
};


#endif