#ifndef __CSCORECOUNTER_H__
#define __CSCORECOUNTER_H__

#include "cocos2d.h"
#include "CMushroomController.h"

USING_NS_CC;

class CScoreCounter
{
private:
	int m_iScore;
	CMushroomController *m_pMushroomController;
	cocos2d::CCLabelBMFont *m_pScoreLabel;
	char m_cScore[9]; // 必須是字串的形式, 因此結尾必須有 null character, 顯示8個字元, 所以必須有 9 個字元
	
public:
	CScoreCounter(cocos2d::CCLayer &inlayer);
	void setMushroomController(CMushroomController *mushroomcontroller);
	void showScore();
	void TouchesBegin(cocos2d::CCPoint inPos);
	void TouchesMoved(cocos2d::CCPoint inPos); 
	void TouchesEnded(cocos2d::CCPoint inPos); 
	int getScore();

};


#endif