#include "CMushroom.h"

CMushroom::CMushroom()
{

}

void CMushroom::SetInfo(const char *NormalPic, cocos2d::CCLayer &inlayer, cocos2d::CCPoint inPos)
{
	m_pSinglePic = CCSprite::createWithSpriteFrameName(NormalPic);
	//m_pSinglePic = CCSprite::create(NormalPic);
	m_PicPos = inPos;
	m_pSinglePic->setPosition(m_PicPos); // 設定位置
	m_pSinglePic->setColor(ccWHITE);  // m_pSinglePic->setColor(ccc3(128,0,255));
	m_fDeg = 0.0f; 
	m_fTime = 0.0f;
	m_fTimeStaying=0.0f;
	m_pSinglePic->setRotation(m_fDeg);   // 順時針方向旋轉, m_pSinglePic->setRotationY(90);
	m_pSinglePic->setScale(1.0);   // 縮放
	m_pSinglePic->setOpacity(255);  // 透明度 0 ~ 255(完全不透明)
	inlayer.addChild(m_pSinglePic,1); // 加入目前的 Layer 中 1: Z 軸的層次，越大代表在越上層
	m_bRot = false;

	// 取得大小
	m_PicSize = m_pSinglePic->getContentSize();

	// 設定判斷區域
	m_PicRect.size = m_PicSize;
	m_PicRect.origin.x = m_PicPos.x - m_PicSize.width*0.5f;
	m_PicRect.origin.y = m_PicPos.y - m_PicSize.height*0.5f;

	m_bTouched = false;
}

void CMushroom::OnFrameMove(CTimer &timer)
{
	if(m_bVisible)
	{	
		m_fTimeStaying+=timer.getDeltaTime();
		if(m_fTimeStaying>=m_fTime)//停留時間超過
		{
			kill();
		}
	}
	
}

int CMushroom::TouchesBegin(cocos2d::CCPoint inPos)
{
	CCLog("aaaaaaaaaaaaaaaaaaaaaaaaaaaa%f",m_fTime);
	if( m_PicRect.containsPoint(inPos) ) //被打到惹
	{
		//CCLog("%f",m_fTime);
		m_bTouched = true;
		m_pSinglePic->setScale(1.25f);   // 縮放
		if(m_fTime<0.5)return(-1);
		
		float ftime = 10.0f - m_fTime;
		int itime=(int)ftime;
		
		kill();
		//setVisible(false);
		//if(time<0.5)return(-1);
		CCLog("Tapped,ftime is %f, and itime is %d",ftime,itime);
		return itime; // 回傳分數
		
	}
	return(0);
}

int CMushroom::TouchesMoved(cocos2d::CCPoint inPos)
{
	if( m_bTouched ) { // 只有被按住的時候才處理
		if( !m_PicRect.containsPoint(inPos) ) { // 手指頭位置離開按鈕
			m_bTouched = false;
			m_pSinglePic->setScale(1.0f);   // 縮放
			return(false);
		}
		else return(0);
	}
	return(0); // 是後移到按鈕上將被忽略
}

int CMushroom::TouchesEnded(cocos2d::CCPoint inPos)
{
	if( m_bTouched )
	{
		m_bTouched = false;
		m_pSinglePic->setScale(1.0f);   // 縮放
		if( m_PicRect.containsPoint(inPos) )  return(0);  // 手指頭位置按鈕時，還在該按鈕上
	}
	return (0);
}

void CMushroom::setVisible(bool bVisible)
{
	m_bVisible=bVisible;
	m_pSinglePic->setVisible(m_bVisible);
}

bool CMushroom::getVisible()
{
	return(m_bVisible);
}

void CMushroom::pop()
{
	m_fTime = rand()%5+1;
	m_fTimeStaying=0.0f;
	setVisible(true);
}

void CMushroom::kill()
{
	m_fTimeStaying=0.0f;
	m_fTime=0.0f;
	setVisible(false);
}
