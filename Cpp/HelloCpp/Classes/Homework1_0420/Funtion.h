float Lerp(float start,float end,float lerp)
{
	if(lerp<=0)return(start);
	else if(lerp>=1)return(end);
	else return(start+(end-start)*lerp);
}