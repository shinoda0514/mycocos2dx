﻿#include "AppMacros.h"
#include "CHomeworkScene.h"



USING_NS_CC;

CCScene* CHomeworkScene::scene()
{
    // 'scene' is an autorelease object
	CCScene *scene = CCScene::create();
    if( scene == NULL ) {
		// Scene 開啟失敗
		cocos2d::CCLog("CHomeworkScene 無法建立");
		return(NULL);
	}
    // Scenelayer' is an autorelease object
    CHomeworkScene *layer = CHomeworkScene::create();
	
    // add layer as a child to scene
    scene->addChild(layer,0); // SceneLayer 預設都在 z:0

	return scene;
}

bool CHomeworkScene::init()
{
	// 1. super init first
    if ( !CCLayer::init() )  { return false; }

	// Set Background Picture. 如果背景圖不會改變, 可以使用以下的方式
    CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
    CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

	// Initialize Background sprite, set the position and add it to this layer.
	this->m_pBackgroundPic = CCSprite::create("MultiScenes/background.png");
	this->m_pBackgroundPic->setPosition(ccp(visibleSize.width	/ 2, visibleSize.height / 2));
	this->addChild(this->m_pBackgroundPic, 0);
	

	// 讀入 plist 定義檔
    //CCSpriteFrameCache *frameCache = CCSpriteFrameCache::sharedSpriteFrameCache();
    // frameCache->addSpriteFramesWithFile("MultiScenes/firstscene.plist");

	// 動態圖檔的置入，The texture will be associated with the created sprite frames.
    CCSpriteFrameCache *frameCache = CCSpriteFrameCache::sharedSpriteFrameCache();
	frameCache->addSpriteFramesWithFile("MultiScenes/firstscene.plist"); 
	frameCache->addSpriteFramesWithFile("MultiScenes/mushroom.plist","MultiScenes/mushroom.png"); 
	//frameCache->addSpriteFramesWithFile("MultiScenes/firstscene.plist"); 
		
	// add a label shows "First Scene" 
	CCLabelTTF* pLabel = CCLabelTTF::create("The First Scene", "Arial", TITLE_FONT_SIZE); 
//  CCLabelTTF* pLabel = CCLabelTTF::create("The First Scene", "Arial", 32); 
    // position the label on the center of the screen
    pLabel->setPosition(ccp(origin.x +150,
                            origin.y + visibleSize.height - pLabel->getContentSize().height));
	// add the label as a child to this layer
    this->addChild(pLabel, 0);
	// Changing the string is as expensive as creating a new CCLabelTTF. To obtain better performance use CCLabelBMFont

	m_pGameOverLabel = CCLabelTTF::create("Game Over", "Arial", 100); 
//  CCLabelTTF* pLabel = CCLabelTTF::create("The First Scene", "Arial", 32); 
    // position the label on the center of the screen
	m_pGameOverLabel->setPosition(ccp(visibleSize.width/2,visibleSize.height/2));
	// add the label as a child to this layer
    this->addChild(m_pGameOverLabel, 0);
	m_pGameOverLabel->setVisible(false);
	m_bIsGameOver=false;

	


	// 設定切換場景所需的參數
	m_pRestartBtn = new CButton;
	m_pRestartBtn->SetButtonInfo("redbtn.png", *this, ccp(950,60));
	m_bSwitchScene = false;

	//設定暫停鈕
	m_pPauseBtn = new CButton;
	m_pPauseBtn->SetButtonInfo("redbtn.png", *this, ccp(visibleSize.width-50,visibleSize.height-50));
	m_bPlaying=false;

	//Timer
	m_pTimer=new CTimer(*this);

	//mush
	m_pMushroomController=new CMushroomController;
	m_pMushroomController->init(*this);
	m_pScoreCounter=new CScoreCounter(*this);
	m_pScoreCounter->setMushroomController(m_pMushroomController);

	// 記憶體的釋放
	frameCache->removeSpriteFramesFromFile("MultiScenes/firstscene.plist");
	frameCache->removeSpriteFramesFromFile("MultiScenes/mushroom.plist");

	
	this->schedule(schedule_selector(CHomeworkScene::OnFrameMove) );
	this->setTouchEnabled(true);
	return true;
}

void CHomeworkScene::OnFrameMove(float dt)
{

	if(m_bPlaying && !m_bIsGameOver)
	{
		m_pTimer->setTimer(dt);
		m_pMushroomController->OnFrameMove(*m_pTimer);
		m_pScoreCounter->showScore();
		m_pTimer->showTime();
	}
	if(m_pTimer->getTime()>=10)
	{
		m_bIsGameOver=true;
		m_pGameOverLabel->setVisible(true);
	}

	if( m_bSwitchScene ) { // 切換到 SecondScene
		// 先將這個 SCENE 的 Update(這裡使用 OnFrameMove, 從 schedule update 中移出)
		this->unschedule(schedule_selector(CHomeworkScene::OnFrameMove)); 
		// 設定場景切換的特效
		CCTransitionPageTurn *pageTurn = CCTransitionPageTurn::create(1.0F, CHomeworkScene::scene(), false);
		CCDirector::sharedDirector()->replaceScene(pageTurn);
//		CocosDenshion::SimpleAudioEngine::sharedEngine()->stopBackgroundMusic(true);
	}
	
	
}

void CHomeworkScene::ccTouchesBegan(CCSet *touches, CCEvent *events)
{
    CCTouch *touch = (CCTouch*)touches->anyObject();
	CCPoint touchLocation = touch->getLocationInView(); // 螢幕座標,左上角是 (0,0) ,座標是整數以 PIXEL 為單位
	CCPoint touchPoint = CCDirector::sharedDirector()->convertToGL(touchLocation); // 轉換到OPENGL 的座標, 右下角是 (0,0) ,座標是浮點數 

	if( m_pRestartBtn->TouchesBegin(touchPoint) ) { // 按到就進行場景切換
		m_bSwitchScene = true;
	}
	if(!m_bIsGameOver)
	{
		if(m_pPauseBtn->TouchesBegin(touchPoint) )
		{
			m_bPlaying=!m_bPlaying;
		}
		if(m_bPlaying)
		{
			//m_pMushroomController->TouchesBegin(touchPoint);
			m_pScoreCounter->TouchesBegin(touchPoint);
		}
	}
}

void CHomeworkScene::ccTouchesMoved(CCSet *touches, CCEvent *events)
{
    CCTouch *touch = (CCTouch*)touches->anyObject();
    CCPoint touchLocation = touch->getLocationInView();
	CCPoint touchPoint = CCDirector::sharedDirector()->convertToGL(touchLocation);

	m_pRestartBtn->TouchesEnded(touchPoint);
	if(!m_bIsGameOver)
	{
		m_pPauseBtn->TouchesEnded(touchPoint);
		if(m_bPlaying)
		{
			m_pMushroomController->TouchesMoved(touchPoint);
		}
	}
}

void CHomeworkScene::ccTouchesEnded(CCSet* touches, CCEvent* event)
{
    CCTouch *touch = (CCTouch*)touches->anyObject();
    CCPoint touchLocation = touch->getLocationInView();
	CCPoint touchPoint = CCDirector::sharedDirector()->convertToGL(touchLocation);

	m_pRestartBtn->TouchesEnded(touchPoint);
	if(!m_bIsGameOver)
	{
		m_pPauseBtn->TouchesEnded(touchPoint);
		if(m_bPlaying)
		{		
			m_pMushroomController->TouchesEnded(touchPoint);
		}
	}
		


}